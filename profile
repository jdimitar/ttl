<?xml version="1.0" encoding="UTF-8"?>
<rdf:RDF xmlns:cims="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#" xmlns:cim="http://iec.ch/TC57/2013/CIM-schema-cim16#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xml:base="http://iec.ch/TC57/2013/CIM-schema-cim16">
	 <rdf:Description rdf:about="#Package_Profil spored model na Miha od ELLJ">
	<rdfs:label xml:lang="en">Profil spored model na Miha od ELLJ</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
</rdf:Description>
	 <rdf:Description rdf:about="#Package_IEC61968">
	<rdfs:label xml:lang="en">IEC61968</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">The IEC 61968 subpackages of the CIM are developed, standardized and maintained by IEC TC57 Working Group 14: interfaces for distribution management (WG14).
Currently, normative parts of the model support the needs of information exchange defined in IEC 61968-3, IEC 61968-4, IEC 61968-9 and in IEC 61968-13.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#Package_IEC61970">
	<rdfs:label xml:lang="en">IEC61970</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">Top package for IEC 61970.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#Package_AssetInfo">
	<rdfs:label xml:lang="en">AssetInfo</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">This package is an extension of Assets package and contains the core information classes that support asset management and different network and work planning applications with specialized AssetInfo subclasses. They hold attributes that can be referenced by not only Asset-s or AssetModel-s but also by ConductingEquipment-s.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#CableInfo">
	<rdfs:label xml:lang="en">CableInfo</rdfs:label>
	<rdfs:subClassOf rdf:resource="#WireInfo"/>
	<rdfs:comment  rdf:parseType="Literal">Cable data.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_AssetInfo"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#CableInfo"/>
	<cims:inverseRoleName rdf:resource="#CableInfo.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#CableInfo.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#CableInfo"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#CIMDataObject.PropertiesObject">
	<rdfs:label xml:lang="en">PropertiesObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The CIM object holding the properties of this dataset context.   Sometimes properties are not required and only the reference to the registered object is required.</rdfs:comment>
	<rdfs:domain rdf:resource="#CIMDataObject"/>
	<rdfs:range rdf:resource="#CableInfo"/>
	<cims:inverseRoleName rdf:resource="#CableInfo.PropertiesCIMDataObject"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
<cims:stereotype>informative</cims:stereotype>
	 </rdf:Description>
	 <rdf:Description rdf:about="#CableInfo.PropertiesCIMDataObject">
	<rdfs:label xml:lang="en">PropertiesCIMDataObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The single CIM data object in the appropriate dataset context.</rdfs:comment>
	<rdfs:domain rdf:resource="#CableInfo"/>
	<rdfs:range rdf:resource="#CIMDataObject"/>
	<cims:inverseRoleName rdf:resource="#CIMDataObject.PropertiesObject"/>
	<rdfs:label xml:lang="en">PropertiesCIMDataObject</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
<cims:stereotype>informative</cims:stereotype>
	 </rdf:Description>
	 <rdf:Description rdf:about="#OverheadWireInfo">
	<rdfs:label xml:lang="en">OverheadWireInfo</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Overhead wire data.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_AssetInfo"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#OverheadWireInfo.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#OverheadWireInfo"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#String">
	<rdfs:label xml:lang="en">String</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">A string consisting of a sequence of characters. The character encoding is UTF-8. The string length is unspecified and unlimited.</rdfs:comment>
<cims:stereotype>Primitive</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#OverheadWireInfo.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#OverheadWireInfo"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#OverheadWireInfo.ratedCurrent">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ratedCurrent</rdfs:label>
	<rdfs:domain rdf:resource="#OverheadWireInfo"/>
	<cims:dataType rdf:resource="#CurrentFlow"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Current carrying capacity of the wire under stated thermal conditions.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#CurrentFlow">
	<rdfs:label xml:lang="en">CurrentFlow</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Electrical current with sign convention: positive flow is out of the conducting equipment into the connectivity node. Can be both AC and DC.</rdfs:comment>
<cims:stereotype>CIMDatatype</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#CurrentFlow.unit">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">unit</rdfs:label>
	<rdfs:domain rdf:resource="#CurrentFlow"/>
	<cims:dataType rdf:resource="#UnitSymbol"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<cims:isFixed rdfs:Literal="A" />
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol">
	<rdfs:label xml:lang="en">UnitSymbol</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The units defined for usage in the CIM.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.m">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="2" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Length in meter.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.kg">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">kg</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="3" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Mass in kilogram.  Note: multiplier “k” is included in this unit symbol for compatibility with IEC 61850-7-3.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.s">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">s</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="4" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Time in seconds.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.A">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">A</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="5" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Current in Ampere.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.K">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">K</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="6" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Temperature in Kelvin.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.mol">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mol</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="7" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Amount of substance in mole.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.cd">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">cd</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="8" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Luminous intensity in candela.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.H">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">H</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="28" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Electric inductance in Henry (Wb/A).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.V">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">V</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="29" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Electric potential in Volt (W/A).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.ohm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ohm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="30" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Electric resistance in ohm (V/A).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.J">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">J</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="31" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Energy in joule (N·m = C·V = W·s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.N">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">N</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="32" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Force in Newton (kg·m/s²).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Hz">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Hz</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="33" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Frequency in Hertz (1/s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.lx">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">lx</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="34" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Illuminance in lux (lm/m²).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.lm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">lm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="35" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Luminous flux in lumen (cd·sr).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Wb">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Wb</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="36" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Magnetic flux in Weber (V·s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.T">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">T</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="37" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Magnetic flux density in Tesla (Wb/m2).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.W">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">W</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="38" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Real power in Watt (J/s). Electrical power may have real and reactive components. The real portion of electrical power (I²R or VIcos(phi)), is expressed in Watts. (See also apparent power and reactive power.)</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.rotPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">rotPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="53" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Rotations per second (1/s). See also Hz (1/s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.radPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">radPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="54" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Angular velocity in radians per second (rad/s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.VA">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">VA</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="61" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Apparent power in Volt Ampere (See also real power and reactive power.)</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.VAr">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">VAr</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="63" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Reactive power in Volt Ampere reactive. The “reactive” or “imaginary” component of electrical power (VIsin(phi)). (See also real power and apparent power).
Note: Different meter designs use different methods to arrive at their results. Some meters may compute reactive power as an arithmetic value, while others compute the value vectorially. The data consumer should determine the method in use and the suitability of the measurement for the intended purpose.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.cosPhi">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">cosPhi</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="65" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Power factor, dimensionless.
Note 1: This definition of power factor only holds for balanced systems. See the alternative definition under code 153.
Note 2 : Beware of differing sign conventions in use between the IEC and EEI. It is assumed that the data consumer understands the type of meter in use and the sign convention in use by the utility.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Vs">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Vs</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="66" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volt second (Ws/A).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.V2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">V2</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="67" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volt squared (W²/A²).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.As">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">As</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="68" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Ampere seconds (A·s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.A2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">A2</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="69" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Ampere squared (A²).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.A2s">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">A2s</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="70" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Ampere squared time in square ampere (A²s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.VAh">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">VAh</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="71" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Apparent energy in Volt Ampere hours.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.dBm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">dBm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="83" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Power level (logrithmic ratio of signal strength , Bel-mW), normalized to 1mW. Note:  multiplier “d” is included in this unit symbol for compatibility with IEC 61850-7-3.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.h">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">h</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="84" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Time, hour = 60 min = 3600 s.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.min">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">min</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="85" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Time, minute  = 60 s.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Q">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Q</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="100" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Quantity power, Q.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Qh">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Qh</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="101" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Quantity energy, Qh.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.ohmm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ohmm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="102" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">resistivity, Ohm metre, (rho).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.APerm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">APerm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="103" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">A/m, magnetic field strength, Ampere per metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.V2h">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">V2h</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="104" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">volt-squared hour, Volt-squared-hours.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.A2h">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">A2h</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="105" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">ampere-squared hour, Ampere-squared hour.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Ah">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Ah</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="106" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Ampere-hours, Ampere-hours.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.count">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">count</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="111" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Amount of substance, Counter value.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.deg">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">deg</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="9" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Plane angle in degrees.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.rad">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">rad</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="10" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Plane angle in radian (m/m).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.sr">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">sr</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="11" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Solid angle in steradian (m2/m2).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Gy">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Gy</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="21" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Absorbed dose in Gray (J/kg).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Bq">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Bq</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="22" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Radioactivity in Becquerel (1/s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.degC">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">degC</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="23" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Relative temperature in degrees Celsius.
In the SI unit system the symbol is ºC. Electric charge is measured in coulomb that has the unit symbol C. To distinguish degree Celsius form coulomb the symbol used in the UML is degC. Reason for not using ºC is the special character º is difficult to manage in software.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Sv">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Sv</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="24" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Dose equivalent in Sievert (J/kg).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.F">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">F</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="25" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Electric capacitance in Farad (C/V).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.C">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">C</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="26" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Electric charge in Coulomb (A·s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.S">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">S</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="27" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Conductance in Siemens.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Pa">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Pa</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="39" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Pressure in Pascal (N/m²). Note: the absolute or relative measurement of pressure is implied with this entry. See below for more explicit forms.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.m2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m2</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="41" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Area in square metre (m²).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.m3">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m3</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="42" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volume in cubic metre (m³).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.mPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="43" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Velocity in metre per second (m/s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.mPers2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mPers2</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="44" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Acceleration in metre per second squared (m/s²).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.m3Pers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m3Pers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="45" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volumetric flow rate in cubic metres per second (m³/s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.mPerm3">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mPerm3</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="46" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Fuel efficiency in metre per cubic metre (m/m³).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.kgm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">kgm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="47" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Moment of mass in kilogram metre (kg·m) (first moment of mass). Note: multiplier “k” is included in this unit symbol for compatibility with IEC 61850-7-3.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.kgPerm3">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">kgPerm3</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="48" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Density in kilogram/cubic metre (kg/m³). Note: multiplier “k” is included in this unit symbol for compatibility with IEC 61850-7-3.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.WPermK">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">WPermK</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="50" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Thermal conductivity in Watt/metre Kelvin.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.JPerK">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">JPerK</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="51" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Heat capacity in Joule/Kelvin.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.ppm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ppm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="52" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Concentration in parts per million.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Wh">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Wh</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="72" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Real energy in Watt hours.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.VArh">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">VArh</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="73" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Reactive energy in Volt Ampere reactive hours.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.VPerHz">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">VPerHz</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="74" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Magnetic flux in Volt per Hertz.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.HzPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">HzPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="75" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Rate of change of frequency in Hertz per second.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.character">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">character</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="76" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Number of characters.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.charPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">charPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="77" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Data rate (baud) in characters per second.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.kgm2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">kgm2</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="78" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Moment of mass in kilogram square metre (kg·m²) (Second moment of mass, commonly called the moment of inertia). Note: multiplier “k” is included in this unit symbol for compatibility with IEC 61850-7-3.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.dB">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">dB</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="79" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Sound pressure level in decibel. Note:  multiplier “d” is included in this unit symbol for compatibility with IEC 61850-7-3.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.WPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">WPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="81" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Ramp rate in Watt per second.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.lPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">lPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="82" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volumetric flow rate in litre per second.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.ft3">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ft3</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="119" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volume, cubic foot.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.m3Perh">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m3Perh</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="125" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volumetric flow rate, cubic metre per hour.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.gal">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">gal</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="128" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volume, US gallon (1 gal = 231 in3 = 128 fl ounce).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Btu">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Btu</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="132" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Energy, British Thermal Unit.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.l">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">l</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="134" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volume, litre = dm3 = m3/1000.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.lPerh">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">lPerh</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="137" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volumetric flow rate, litre per hour.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.lPerl">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">lPerl</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="143" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Concentration, The ratio of the volume of a solute divided by the volume of  the solution. Note: Users may need use a prefix such a ‘µ’ to express a quantity such as ‘µL/L’.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.gPerg">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">gPerg</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="144" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Concentration, The ratio of the mass of a solute divided by the mass of  the solution. Note: Users may need use a prefix such a ‘µ’ to express a quantity such as ‘µg/g’.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.molPerm3">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">molPerm3</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="145" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Concentration, The amount of substance concentration, (c), the amount of solvent in moles divided by the volume of solution in m³.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.molPermol">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">molPermol</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="146" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Concentration, Molar fraction (?), the ratio of the molar amount of a solute divided by the molar amount of the solution.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.molPerkg">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">molPerkg</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="147" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Concentration, Molality, the amount of solute in moles and the amount of solvent in kilograms.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.sPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">sPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="149" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Time, Ratio of time Note: Users may need to supply a prefix such as ‘µ’ to show rates such as ‘µs/s’</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.HzPerHz">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">HzPerHz</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="150" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Frequency, Rate of frequency change  Note: Users may need to supply a prefix such as ‘m’ to show rates such as ‘mHz/Hz’.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.VPerV">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">VPerV</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="151" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Voltage, Ratio of voltages Note: Users may need to supply a prefix such as ‘m’ to show rates such as ‘mV/V’.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.APerA">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">APerA</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="152" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Current, Ratio of Amperages  Note: Users may need to supply a prefix such as ‘m’ to show rates such as ‘mA/A’.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.VPerVA">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">VPerVA</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="153" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Power factor, PF, the ratio of the active power to the apparent power. Note: The sign convention used for power factor will differ between IEC meters and EEI (ANSI) meters. It is assumed that the data consumers understand the type of meter being used and agree on the sign convention in use at any given utility.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.rev">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">rev</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="154" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Amount of rotation, Revolutions.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.kat">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">kat</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="158" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Catalytic activity, katal = mol / s.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.JPerkg">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">JPerkg</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="165" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Specific energy, Joule / kg.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.m3Uncompensated">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m3Uncompensated</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="166" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volume, cubic metre, with the value uncompensated for weather effects.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.m3Compensated">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m3Compensated</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="167" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volume, cubic metre, with the value compensated for weather effects.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.WPerW">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">WPerW</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="168" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Signal Strength, Ratio of power  Note: Users may need to supply a prefix such as ‘m’ to show rates such as ‘mW/W’.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.therm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">therm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="169" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Energy, Therm.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.onePerm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">onePerm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="173" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Wavenumber, reciprocal metre,  (1/m).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.m3Perkg">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m3Perkg</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="174" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Specific volume, cubic metre per kilogram, v.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Pas">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Pas</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="175" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Dynamic viscosity, Pascal second.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Nm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Nm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="176" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Moment of force, Newton metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.NPerm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">NPerm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="177" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Surface tension, Newton per metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.radPers2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">radPers2</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="178" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Angular acceleration, radian per second squared.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.WPerm2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">WPerm2</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="55" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Heat flux density, irradiance, Watt per square metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.JPerkgK">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">JPerkgK</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="60" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Specific heat capacity, specific entropy, Joule per kilogram Kelvin.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.JPerm3">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">JPerm3</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="181" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">energy density, Joule per cubic metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.VPerm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">VPerm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="182" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">electric field strength, Volt per metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.CPerm3">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">CPerm3</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="183" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">electric charge density, Coulomb per cubic metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.CPerm2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">CPerm2</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="184" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">surface charge density, Coulomb per square metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.FPerm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">FPerm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="185" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">permittivity, Farad per metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.HPerm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">HPerm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="186" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">permeability, Henry per metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.JPermol">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">JPermol</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="187" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">molar energy, Joule per mole.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.JPermolK">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">JPermolK</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="188" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">molar entropy, molar heat capacity, Joule per mole kelvin.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.CPerkg">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">CPerkg</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="189" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">exposure (x rays), Coulomb per kilogram.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.GyPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">GyPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="190" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">absorbed dose rate, Gray per second.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.WPersr">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">WPersr</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="191" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Radiant intensity, Watt per steradian.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.WPerm2sr">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">WPerm2sr</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="192" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">radiance, Watt per square metre steradian.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.katPerm3">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">katPerm3</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="193" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">catalytic activity concentration, katal per cubic metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.G">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">G</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="277" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Magnetic flux density, Gauss (1 G = 10-4 T).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Oe">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Oe</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="278" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Magnetic field, Œrsted (1 Oe = (103/4p) A/m).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.WPerA">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">WPerA</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Active power per current flow, watt per Ampere.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.SPerm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">SPerm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="57" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Conductance per length (F/m).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.onePerHz">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">onePerHz</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Reciprocal of frequency (1/Hz).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.VPerVAr">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">VPerVAr</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Power factor, PF, the ratio of the active power to the apparent power. Note: The sign convention used for power factor will differ between IEC meters and EEI (ANSI) meters. It is assumed that the data consumers understand the type of meter being used and agree on the sign convention in use at any given utility.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.ohmPerm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ohmPerm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Electric resistance per length in ohm per metre ((V/A)/m).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.kgPerJ">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">kgPerJ</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Weigh per energy in kilogram/joule (kg/J). Note: multiplier “k” is included in this unit symbol for compatibility with IEC 61850-7-3.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.JPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">JPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Energy rate joule per second (J/s),</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.m2Pers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m2Pers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="49" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Viscosity in metre square / second (m²/s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.JPerm2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">JPerm2</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="56" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Insulation energy density, Joule per square metre or watt second per square metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.KPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">KPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="58" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Temperature change rate in Kelvin per second.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.d">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">d</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="195" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Time, day = 24 h = 86400 s.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.anglemin">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">anglemin</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="196" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Plane angle, minute.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.anglesec">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">anglesec</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="197" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Plane angle, second.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.ha">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ha</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="198" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Area, hectare.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.tonne">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">tonne</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="199" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">mass, “tonne” or “metric  ton” (1000 kg = 1 Mg).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.bar">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">bar</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="214" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Pressure, bar (1 bar = 100 kPa).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.mmHg">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mmHg</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="215" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Pressure, millimeter of mercury (1 mmHg is approximately 133.3 Pa).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.M">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">M</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="217" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Length, nautical mile (1 M = 1852 m).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.kn">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">kn</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="219" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Speed, knot (1 kn = 1852/3600) m/s.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Vh">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Vh</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="280" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volt-hour, Volt hours.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Mx">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Mx</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="276" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Magnetic flux, Maxwell (1 Mx = 10-8 Wb).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.PaPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">PaPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="59" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Pressure change rate in Pascal per second.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#CurrentFlow.value">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">value</rdfs:label>
	<rdfs:domain rdf:resource="#CurrentFlow"/>
	<cims:dataType rdf:resource="#Float"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Float">
	<rdfs:label xml:lang="en">Float</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">A floating point number. The range is unspecified and not limited.</rdfs:comment>
<cims:stereotype>Primitive</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#CurrentFlow.multiplier">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">multiplier</rdfs:label>
	<rdfs:domain rdf:resource="#CurrentFlow"/>
	<cims:dataType rdf:resource="#UnitMultiplier"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier">
	<rdfs:label xml:lang="en">UnitMultiplier</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The unit multipliers defined for the CIM.  When applied to unit symbols that already contain a multiplier, both multipliers are used. For example, to exchange kilograms using unit symbol of kg, one uses the "none" multiplier, to exchange metric ton (Mg), one uses the "k" multiplier.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.y">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">y</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-24" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">yocto 10**-24.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.z">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">z</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-21" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">zepto 10**-21.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.a">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">a</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-18" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">atto 10**-18.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.f">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">f</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-15" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">femto 10**-15.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.p">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">p</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-12" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Pico 10**-12.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.P">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">P</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="15" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Peta 10**15</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.E">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">E</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="18" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Exa 10**18.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.Y">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Y</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="24" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Yotta 10**24</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.Z">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Z</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="21" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Zetta 10**21</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.n">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">n</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-9" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Nano 10**-9.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.micro">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">micro</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-6" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Micro 10**-6.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.m">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-3" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Milli 10**-3.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.c">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">c</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-2" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Centi 10**-2.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.d">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">d</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Deci 10**-1.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.none">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">none</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="0" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">No multiplier or equivalently multiply by 1.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.da">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">da</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">deca 10**1.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.h">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">h</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="2" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">hecto 10**2.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.k">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">k</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="3" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Kilo 10**3.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.M">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">M</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="6" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Mega 10**6.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.G">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">G</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="9" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Giga 10**9.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.T">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">T</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="12" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Tera 10**12.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#OverheadWireInfo.PerLengthParameters">
	<rdfs:label xml:lang="en">PerLengthParameters</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All per-length parameters calculated from this wire datasheet.</rdfs:comment>
	<rdfs:domain rdf:resource="#OverheadWireInfo"/>
	<rdfs:range rdf:resource="#PerLengthLineParameter"/>
	<cims:inverseRoleName rdf:resource="#PerLengthLineParameter.WireInfos"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PerLengthLineParameter.WireInfos">
	<rdfs:label xml:lang="en">WireInfos</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All wire datasheets used to calculate this per-length parameter.</rdfs:comment>
	<rdfs:domain rdf:resource="#PerLengthLineParameter"/>
	<rdfs:range rdf:resource="#OverheadWireInfo"/>
	<cims:inverseRoleName rdf:resource="#OverheadWireInfo.PerLengthParameters"/>
	<rdfs:label xml:lang="en">WireInfos</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WireInfo">
	<rdfs:label xml:lang="en">WireInfo</rdfs:label>
	<rdfs:subClassOf rdf:resource="#IdentifiedObject"/>
	<rdfs:comment  rdf:parseType="Literal">Wire data that can be specified per line segment phase, or for the line segment as a whole in case its phases all have the same wire characteristics.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_AssetInfo"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WireInfo.ratedCurrent">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ratedCurrent</rdfs:label>
	<rdfs:domain rdf:resource="#WireInfo"/>
	<cims:dataType rdf:resource="#CurrentFlow"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Current carrying capacity of the wire under stated thermal conditions.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WireInfo.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#WireInfo"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WireInfo.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#WireInfo"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WireInfo.material">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">material</rdfs:label>
	<rdfs:domain rdf:resource="#WireInfo"/>
	<cims:dataType rdf:resource="#WireMaterialKind"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Conductor material.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WireMaterialKind">
	<rdfs:label xml:lang="en">WireMaterialKind</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Kind of wire material.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_AssetInfo"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WireInfo.PerLengthParameters">
	<rdfs:label xml:lang="en">PerLengthParameters</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All per-length parameters calculated from this wire datasheet.</rdfs:comment>
	<rdfs:domain rdf:resource="#WireInfo"/>
	<rdfs:range rdf:resource="#PerLengthLineParameter"/>
	<cims:inverseRoleName rdf:resource="#PerLengthLineParameter.WireInfos"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PerLengthLineParameter.WireInfos">
	<rdfs:label xml:lang="en">WireInfos</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All wire datasheets used to calculate this per-length parameter.</rdfs:comment>
	<rdfs:domain rdf:resource="#PerLengthLineParameter"/>
	<rdfs:range rdf:resource="#WireInfo"/>
	<cims:inverseRoleName rdf:resource="#WireInfo.PerLengthParameters"/>
	<rdfs:label xml:lang="en">WireInfos</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WireMaterialKind">
	<rdfs:label xml:lang="en">WireMaterialKind</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Kind of wire material.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_AssetInfo"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_Assets">
	<rdfs:label xml:lang="en">Assets</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">This package contains the core information classes that support asset management applications that deal with the physical and lifecycle aspects of various network resources (as opposed to power system resource models defined in IEC61970::Wires package, which support network applications).</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#Asset">
	<rdfs:label xml:lang="en">Asset</rdfs:label>
	<rdfs:subClassOf rdf:resource="#IdentifiedObject"/>
	<rdfs:comment  rdf:parseType="Literal">Tangible resource of the utility, including power system equipment, various end devices, cabinets, buildings, etc. For electrical network equipment, the role of the asset is defined through PowerSystemResource and its subclasses, defined mainly in the Wires model (refer to IEC61970-301 and model package IEC61970::Wires). Asset description places emphasis on the physical characteristics of the equipment fulfilling that role.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Assets"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Asset.serialNumber">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">serialNumber</rdfs:label>
	<rdfs:domain rdf:resource="#Asset"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Serial number of this asset.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Asset.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#Asset"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Asset.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#Asset"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Asset.Measurements">
	<rdfs:label xml:lang="en">Measurements</rdfs:label>
	<rdfs:domain rdf:resource="#Asset"/>
	<rdfs:range rdf:resource="#Measurement"/>
	<cims:inverseRoleName rdf:resource="#Measurement.Asset"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Measurement.Asset">
	<rdfs:label xml:lang="en">Asset</rdfs:label>
	<rdfs:domain rdf:resource="#Measurement"/>
	<rdfs:range rdf:resource="#Asset"/>
	<cims:inverseRoleName rdf:resource="#Asset.Measurements"/>
	<rdfs:label xml:lang="en">Asset</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#AssetInfo">
	<rdfs:label xml:lang="en">AssetInfo</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Set of attributes of an asset, representing typical datasheet information of a physical device that can be instantiated and shared in different data exchange contexts:
- as attributes of an asset instance (installed or in stock)
- as attributes of an asset model (product by a manufacturer)
- as attributes of a type asset (generic type of an asset as used in designs/extension planning).</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Assets"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#AssetInfo.aliasName">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">aliasName</rdfs:label>
	<rdfs:domain rdf:resource="#AssetInfo"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The aliasName is free text human readable name of the object alternative to IdentifiedObject.name. It may be non unique and may not correlate to a naming hierarchy.
The attribute aliasName is retained because of backwards compatibility between CIM relases. It is however recommended to replace aliasName with the Name class as aliasName is planned for retirement at a future time.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#AssetInfo.description">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">description</rdfs:label>
	<rdfs:domain rdf:resource="#AssetInfo"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The description is a free human readable text describing or naming the object. It may be non unique and may not correlate to a naming hierarchy.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#AssetInfo.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#AssetInfo"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#AssetInfo.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#AssetInfo"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_Common">
	<rdfs:label xml:lang="en">Common</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">This package contains the information classes that support distribution management in general.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#CoordinateSystem">
	<rdfs:label xml:lang="en">CoordinateSystem</rdfs:label>
	<rdfs:subClassOf rdf:resource="#IdentifiedObject"/>
	<rdfs:comment  rdf:parseType="Literal">Coordinate reference system.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Common"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#CoordinateSystem.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#CoordinateSystem"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#CoordinateSystem.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#CoordinateSystem"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Location.CoordinateSystem">
	<rdfs:label xml:lang="en">CoordinateSystem</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Coordinate system used to describe position points of this location.</rdfs:comment>
	<rdfs:domain rdf:resource="#Location"/>
	<rdfs:range rdf:resource="#CoordinateSystem"/>
	<cims:inverseRoleName rdf:resource="#CoordinateSystem.Locations"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#CoordinateSystem.Locations">
	<rdfs:label xml:lang="en">Locations</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All locations described with position points in this coordinate system.</rdfs:comment>
	<rdfs:domain rdf:resource="#CoordinateSystem"/>
	<rdfs:range rdf:resource="#Location"/>
	<cims:inverseRoleName rdf:resource="#Location.CoordinateSystem"/>
	<rdfs:label xml:lang="en">Locations</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#CoordinateSystem"/>
	<cims:inverseRoleName rdf:resource="#CoordinateSystem.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#CoordinateSystem.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#CoordinateSystem"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#CIMDataObject.PropertiesObject">
	<rdfs:label xml:lang="en">PropertiesObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The CIM object holding the properties of this dataset context.   Sometimes properties are not required and only the reference to the registered object is required.</rdfs:comment>
	<rdfs:domain rdf:resource="#CIMDataObject"/>
	<rdfs:range rdf:resource="#CoordinateSystem"/>
	<cims:inverseRoleName rdf:resource="#CoordinateSystem.PropertiesCIMDataObject"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
<cims:stereotype>informative</cims:stereotype>
	 </rdf:Description>
	 <rdf:Description rdf:about="#CoordinateSystem.PropertiesCIMDataObject">
	<rdfs:label xml:lang="en">PropertiesCIMDataObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The single CIM data object in the appropriate dataset context.</rdfs:comment>
	<rdfs:domain rdf:resource="#CoordinateSystem"/>
	<rdfs:range rdf:resource="#CIMDataObject"/>
	<cims:inverseRoleName rdf:resource="#CIMDataObject.PropertiesObject"/>
	<rdfs:label xml:lang="en">PropertiesCIMDataObject</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
<cims:stereotype>informative</cims:stereotype>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Location">
	<rdfs:label xml:lang="en">Location</rdfs:label>
	<rdfs:subClassOf rdf:resource="#IdentifiedObject"/>
	<rdfs:comment  rdf:parseType="Literal">The place, scene, or point of something where someone or something has been, is, and/or will be at a given moment in time. It can be defined with one or more postition points (coordinates) in a given coordinate system.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Common"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Location.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#Location"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Location.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#Location"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Location.PowerSystemResources">
	<rdfs:label xml:lang="en">PowerSystemResources</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All power system resources at this location.</rdfs:comment>
	<rdfs:domain rdf:resource="#Location"/>
	<rdfs:range rdf:resource="#ACLineSegment"/>
	<cims:inverseRoleName rdf:resource="#ACLineSegment.Location"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ACLineSegment.Location">
	<rdfs:label xml:lang="en">Location</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Location of this power system resource.</rdfs:comment>
	<rdfs:domain rdf:resource="#ACLineSegment"/>
	<rdfs:range rdf:resource="#Location"/>
	<cims:inverseRoleName rdf:resource="#Location.PowerSystemResources"/>
	<rdfs:label xml:lang="en">Location</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#Location"/>
	<cims:inverseRoleName rdf:resource="#Location.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Location.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#Location"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Location.PositionPoints">
	<rdfs:label xml:lang="en">PositionPoints</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Sequence of position points describing this location, expressed in coordinate system 'Location.CoordinateSystem'.</rdfs:comment>
	<rdfs:domain rdf:resource="#Location"/>
	<rdfs:range rdf:resource="#PositionPoint"/>
	<cims:inverseRoleName rdf:resource="#PositionPoint.Location"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PositionPoint.Location">
	<rdfs:label xml:lang="en">Location</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Location described by this position point.</rdfs:comment>
	<rdfs:domain rdf:resource="#PositionPoint"/>
	<rdfs:range rdf:resource="#Location"/>
	<cims:inverseRoleName rdf:resource="#Location.PositionPoints"/>
	<rdfs:label xml:lang="en">Location</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PositionPoint">
	<rdfs:label xml:lang="en">PositionPoint</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Set of spatial coordinates that determine a point, defined in the coordinate system specified in 'Location.CoordinateSystem'. Use a single position point instance to desribe a point-oriented location. Use a sequence of position points to describe a line-oriented object (physical location of non-point oriented objects like cables or lines), or area of an object (like a substation or a geographical zone - in this case, have first and last position point with the same values).</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Common"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PositionPoint.xPosition">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">xPosition</rdfs:label>
	<rdfs:domain rdf:resource="#PositionPoint"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">X axis position.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PositionPoint.yPosition">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">yPosition</rdfs:label>
	<rdfs:domain rdf:resource="#PositionPoint"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Y axis position.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PositionPoint.sequenceNumber">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">sequenceNumber</rdfs:label>
	<rdfs:domain rdf:resource="#PositionPoint"/>
	<cims:dataType rdf:resource="#Integer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Zero-relative sequence number of this point within a series of points.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Integer">
	<rdfs:label xml:lang="en">Integer</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">An integer number. The range is unspecified and not limited.</rdfs:comment>
<cims:stereotype>Primitive</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_InfIEC61968">
	<rdfs:label xml:lang="en">InfIEC61968</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">This package and its subpackages contain informative (unstable) elements of the model, expected to evolve a lot or to be removed, and not published as IEC document yet. Some portions of it will be reworked and moved to normative packages as the need arises, and some portions may be removed.
WG14 does not generate documentation for this informative portion of the model.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#Package_Metering">
	<rdfs:label xml:lang="en">Metering</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">This package contains the core information classes that support end device applications with specialized classes for metering and premises area network devices, and remote reading functions. These classes are generally associated with the point where a service is delivered to the customer.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind">
	<rdfs:label xml:lang="en">MeasurementKind</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Kind of read / measured value.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Metering"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.current">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">current</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="4" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.currentImbalance">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">currentImbalance</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="6" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.currentAngle">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">currentAngle</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="5" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.date">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">date</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="7" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.demand">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">demand</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="8" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.distance">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">distance</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="9" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.distortionVoltAmp">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">distortionVoltAmp</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="10" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.energization">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">energization</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="11" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.energy">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">energy</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="12" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.energizationLoadSide">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">energizationLoadSide</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="13" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.fan">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">fan</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="14" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.frequency">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">frequency</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="15" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.fund">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">fund</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="16" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Dup with “currency”</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.ieee1366ASAI">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ieee1366ASAI</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="17" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.ieee1366ASIDI">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ieee1366ASIDI</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="18" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.ieee1366ASIFI">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ieee1366ASIFI</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="19" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.ieee1366CAIDI">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ieee1366CAIDI</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="20" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.phasorReactivePower">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">phasorReactivePower</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="35" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.none">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">none</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="0" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Not Applicable</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.apparentPowerFactor">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">apparentPowerFactor</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="2" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.currency">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">currency</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="3" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">funds</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.positiveSequence">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">positiveSequence</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="36" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.power">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">power</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="37" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.powerFactor">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">powerFactor</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="38" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.quantityPower">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">quantityPower</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="40" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.swell">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">swell</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="42" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.switchPosition">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">switchPosition</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="43" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.tapPosition">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">tapPosition</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="44" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.tariffRate">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">tariffRate</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="45" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.watchdogTimeout">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">watchdogTimeout</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="149" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.billLastPeriod">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">billLastPeriod</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="150" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Customer’s bill for the previous billing period (Currency)</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.billToDate">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">billToDate</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="151" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Customer’s bill, as known thus far within the present billing period (Currency)</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.ieee1366CAIFI">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ieee1366CAIFI</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="21" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.ieee1366CEMIn">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ieee1366CEMIn</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="22" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.ieee1366CEMSMIn">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ieee1366CEMSMIn</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="23" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.ieee1366CTAIDI">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ieee1366CTAIDI</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="24" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.ieee1366MAIFI">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ieee1366MAIFI</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="25" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.ieee1366MAIFIe">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ieee1366MAIFIe</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="26" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.ieee1366SAIDI">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ieee1366SAIDI</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="27" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.ieee1366SAIFI">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ieee1366SAIFI</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="28" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.lineLoss">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">lineLoss</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="31" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.loss">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">loss</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="32" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.negativeSequence">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">negativeSequence</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="33" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.phasorPowerFactor">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">phasorPowerFactor</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="34" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.temperature">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">temperature</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="46" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.voltage">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">voltage</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="54" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.voltageAngle">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">voltageAngle</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="55" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.voltageExcursion">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">voltageExcursion</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="56" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.voltageImbalance">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">voltageImbalance</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="57" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.volume">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">volume</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="58" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Clarified  from Ed. 1. to indicate fluid volume</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.removalTamper">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">removalTamper</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="143" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.reprogrammingTamper">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">reprogrammingTamper</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="144" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.reverseRotationTamper">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">reverseRotationTamper</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="145" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.switchArmed">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">switchArmed</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="146" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.switchDisabled">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">switchDisabled</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="147" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementKind.tamper">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">tamper</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementKind"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="148" />
	<cims:stereotype>enum</cims:stereotype>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_InfAssets">
	<rdfs:label xml:lang="en">InfAssets</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">The package is used to define asset-level models for objects. Assets may be comprised of other assets and may have relationships to other assets. Assets also have owners and values. Assets may also have a relationship to a PowerSystemResource in the Wires model.

TODO: The following has been copied from a very old version of draft Part 11, so the references are wrong, but we store the knowledge here to reuse later:
"Assets are the basic units which define a physical infrastructure. PowerSystemResources are logical objects meaningful to operations which are constructed from one or more Assets, although PowerSystemResources are not required to specifiy their component Assets.
The Asset package is comprosed of several packages. The key concepts of an Asset are as follows:
<ul>
	<li>Assets can have names, through inheritance to the Naming package</li>
	<li>Assets are physical entities which have a lifecycle</li>
	<li>One or more assets can be associated to create a PowerSystemResource</li>
	<li>Assets can be grouped (aggregated) with other Assets</li>
	<li>Assets are typically either 'point' or 'linear' assets, which relate to physical geometry</li>
	<li>Assets have a close relationship to Work as a consequence of their lifecycle</li>
</ul>
The following sections describe the packages in the Assets package.
The AssetBasics package defines the relationship between Asset and other classes, such as Organization, PowerSystemResource and Document.
Point assets are those assets whose physical location can be described in terms of a single coordinate, such as a pole or a switch.
Linear assets are those assets whose physical location is best described in terms of a line, plyline or polygon.
Asset work triggers are used to determine when inspection and/or maintenance are required for assets".</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#Bushing">
	<rdfs:label xml:lang="en">Bushing</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Bushing asset.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_InfAssets"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Bushing.c1Capacitance">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">c1Capacitance</rdfs:label>
	<rdfs:domain rdf:resource="#Bushing"/>
	<cims:dataType rdf:resource="#Capacitance"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Factory measured capacitance, measured between the power factor tap and the bushing conductor.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Capacitance">
	<rdfs:label xml:lang="en">Capacitance</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Capacitive part of reactance (imaginary part of impedance), at rated frequency.</rdfs:comment>
<cims:stereotype>CIMDatatype</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Bushing.c1PowerFactor">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">c1PowerFactor</rdfs:label>
	<rdfs:domain rdf:resource="#Bushing"/>
	<cims:dataType rdf:resource="#Float"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Factory measured insulation power factor, measured between the power factor tap and the bushing conductor.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Bushing.c2Capacitance">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">c2Capacitance</rdfs:label>
	<rdfs:domain rdf:resource="#Bushing"/>
	<cims:dataType rdf:resource="#Capacitance"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Factory measured capacitance measured between the power factor tap and ground.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Bushing.c2PowerFactor">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">c2PowerFactor</rdfs:label>
	<rdfs:domain rdf:resource="#Bushing"/>
	<cims:dataType rdf:resource="#Float"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Factory measured insulation power factor, measured between the power factor tap and ground.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Bushing.insulationKind">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">insulationKind</rdfs:label>
	<rdfs:domain rdf:resource="#Bushing"/>
	<cims:dataType rdf:resource="#BushingInsulationKind"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Kind of insulation.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BushingInsulationKind">
	<rdfs:label xml:lang="en">BushingInsulationKind</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Insulation kind for bushings.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_InfAssets"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BushingInsulationKind">
	<rdfs:label xml:lang="en">BushingInsulationKind</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Insulation kind for bushings.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_InfAssets"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_Base">
	<rdfs:label xml:lang="en">Base</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
</rdf:Description>
	 <rdf:Description rdf:about="#Package_Dynamics">
	<rdfs:label xml:lang="en">Dynamics</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">The CIM dynamic model definitions reflect the most common IEEE or, in the case of wind models, IEC, representations of models as well as models included in some of the transient stability software widely used by utilities. 

These dynamic models are intended to ensure interoperability between different vendors’ software products currently in use by electric utility energy companies, utilities, TSOs and RTO/ISOs.  It is important to note that each vendor is free to select its own internal implementation of these models.  Differences in vendor results, as long as they are within accepted engineering practice, caused by different internal representations, are acceptable.

Notes:
1. Wind models package is defined in accordance with IEC 61400-27-1 version CDV 2013-08-15.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#Package_InfIEC61970">
	<rdfs:label xml:lang="en">InfIEC61970</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">This package and its subpackages contain informative (unstable) elements of the model, expected to evolve a lot or to be removed, and not published as IEC document yet. Some portions of it will be reworked and moved to normative packages as the need arises, and some portions may be removed.
WG13 does not generate documentation for this informative portion of the model.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#Package_AuxiliaryEquipment">
	<rdfs:label xml:lang="en">AuxiliaryEquipment</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">Contains equipment which is not normal conducting equipment such as sensors, fault locators, and surge protectors.  These devices do not define power carrying topological connections as conducting equipment, but are associated to terminals of other conducting equipment.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#AuxiliaryEquipment">
	<rdfs:label xml:lang="en">AuxiliaryEquipment</rdfs:label>
	<rdfs:subClassOf rdf:resource="#Equipment"/>
	<rdfs:comment  rdf:parseType="Literal">AuxiliaryEquipment describe equipment that is not performing any primary functions but support for the equipment performing the primary function.
AuxiliaryEquipment is attached to primary eqipment via an association with Terminal.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_AuxiliaryEquipment"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#AuxiliaryEquipment.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#AuxiliaryEquipment"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#AuxiliaryEquipment.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#AuxiliaryEquipment"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#AuxiliaryEquipment.Terminal">
	<rdfs:label xml:lang="en">Terminal</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The Terminal at the equipment where the AuxiliaryEquipment is attached.</rdfs:comment>
	<rdfs:domain rdf:resource="#AuxiliaryEquipment"/>
	<rdfs:range rdf:resource="#Terminal"/>
	<cims:inverseRoleName rdf:resource="#Terminal.AuxiliaryEquipment"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Terminal.AuxiliaryEquipment">
	<rdfs:label xml:lang="en">AuxiliaryEquipment</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The auxiliary equipment connected to the terminal.</rdfs:comment>
	<rdfs:domain rdf:resource="#Terminal"/>
	<rdfs:range rdf:resource="#AuxiliaryEquipment"/>
	<cims:inverseRoleName rdf:resource="#AuxiliaryEquipment.Terminal"/>
	<rdfs:label xml:lang="en">AuxiliaryEquipment</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#FaultIndicator">
	<rdfs:label xml:lang="en">FaultIndicator</rdfs:label>
	<rdfs:subClassOf rdf:resource="#IdentifiedObject"/>
	<rdfs:comment  rdf:parseType="Literal">A FaultIndicator is typically only an indicator (which may or may not be remotely monitored), and not a piece of equipment that actually initiates a protection event. It is used for FLISR (Fault Location, Isolation and Restoration) purposes, assisting with the dispatch of crews to "most likely" part of the network (i.e. assists with determining circuit section where the fault most likely happened).</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_AuxiliaryEquipment"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#FaultIndicator.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#FaultIndicator"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#FaultIndicator.Terminal">
	<rdfs:label xml:lang="en">Terminal</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The Terminal at the equipment where the AuxiliaryEquipment is attached.</rdfs:comment>
	<rdfs:domain rdf:resource="#FaultIndicator"/>
	<rdfs:range rdf:resource="#Terminal"/>
	<cims:inverseRoleName rdf:resource="#Terminal.AuxiliaryEquipment"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Terminal.AuxiliaryEquipment">
	<rdfs:label xml:lang="en">AuxiliaryEquipment</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The auxiliary equipment connected to the terminal.</rdfs:comment>
	<rdfs:domain rdf:resource="#Terminal"/>
	<rdfs:range rdf:resource="#FaultIndicator"/>
	<cims:inverseRoleName rdf:resource="#FaultIndicator.Terminal"/>
	<rdfs:label xml:lang="en">AuxiliaryEquipment</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EquipmentContainer.Equipments">
	<rdfs:label xml:lang="en">Equipments</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Contained equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#EquipmentContainer"/>
	<rdfs:range rdf:resource="#FaultIndicator"/>
	<cims:inverseRoleName rdf:resource="#FaultIndicator.EquipmentContainer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#FaultIndicator.EquipmentContainer">
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Container of this equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#FaultIndicator"/>
	<rdfs:range rdf:resource="#EquipmentContainer"/>
	<cims:inverseRoleName rdf:resource="#EquipmentContainer.Equipments"/>
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_ControlArea">
	<rdfs:label xml:lang="en">ControlArea</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">The ControlArea package models area specifications which can be used for a variety of purposes.  The package as a whole models potentially overlapping control area specifications for the purpose of actual generation control, load forecast area load capture, or powerflow based analysis.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#TieFlow">
	<rdfs:label xml:lang="en">TieFlow</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">A flow specification in terms of location and direction for a control area.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_ControlArea"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_Core">
	<rdfs:label xml:lang="en">Core</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">Contains the core PowerSystemResource and ConductingEquipment entities shared by all applications plus common collections of those entities. Not all applications require all the Core entities.  This package does not depend on any other package except the Domain package, but most of the other packages have associations and generalizations that depend on it.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#ACDCTerminal">
	<rdfs:label xml:lang="en">ACDCTerminal</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">An electrical connection point (AC or DC) to a piece of conducting equipment. Terminals are connected at physical connection points called connectivity nodes.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ACDCTerminal.connected">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">connected</rdfs:label>
	<rdfs:domain rdf:resource="#ACDCTerminal"/>
	<cims:dataType rdf:resource="#Boolean"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The connected status is related to a bus-branch model and the topological node to terminal relation.  True implies the terminal is connected to the related topological node and false implies it is not. 
In a bus-branch model, the connected status is used to tell if equipment is disconnected without having to change the connectivity described by the topological node to terminal relation. A valid case is that conducting equipment can be connected in one end and open in the other. In particular for an AC line segment, where the reactive line charging can be significant, this is a relevant case.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Boolean">
	<rdfs:label xml:lang="en">Boolean</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">A type with the value space "true" and "false".</rdfs:comment>
<cims:stereotype>Primitive</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Measurement.Terminal">
	<rdfs:label xml:lang="en">Terminal</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">One or more measurements may be associated with a terminal in the network.</rdfs:comment>
	<rdfs:domain rdf:resource="#Measurement"/>
	<rdfs:range rdf:resource="#ACDCTerminal"/>
	<cims:inverseRoleName rdf:resource="#ACDCTerminal.Measurements"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ACDCTerminal.Measurements">
	<rdfs:label xml:lang="en">Measurements</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Measurements associated with this terminal defining  where the measurement is placed in the network topology.  It may be used, for instance, to capture the sensor position, such as a voltage transformer (PT) at a busbar or a current transformer (CT) at the bar between a breaker and an isolator.</rdfs:comment>
	<rdfs:domain rdf:resource="#ACDCTerminal"/>
	<rdfs:range rdf:resource="#Measurement"/>
	<cims:inverseRoleName rdf:resource="#Measurement.Terminal"/>
	<rdfs:label xml:lang="en">Measurements</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BaseVoltage">
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<rdfs:subClassOf rdf:resource="#IdentifiedObject"/>
	<rdfs:comment  rdf:parseType="Literal">Defines a system base voltage which is referenced. </rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BaseVoltage.nominalVoltage">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">nominalVoltage</rdfs:label>
	<rdfs:domain rdf:resource="#BaseVoltage"/>
	<cims:dataType rdf:resource="#Voltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The power system resource's base voltage.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Voltage">
	<rdfs:label xml:lang="en">Voltage</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Electrical voltage, can be both AC and DC.</rdfs:comment>
<cims:stereotype>CIMDatatype</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Voltage.multiplier">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">multiplier</rdfs:label>
	<rdfs:domain rdf:resource="#Voltage"/>
	<cims:dataType rdf:resource="#UnitMultiplier"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Voltage.value">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">value</rdfs:label>
	<rdfs:domain rdf:resource="#Voltage"/>
	<cims:dataType rdf:resource="#Float"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Voltage.unit">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">unit</rdfs:label>
	<rdfs:domain rdf:resource="#Voltage"/>
	<cims:dataType rdf:resource="#UnitSymbol"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<cims:isFixed rdfs:Literal="V" />
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BaseVoltage.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#BaseVoltage"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BaseVoltage.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#BaseVoltage"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#TransformerEnd.BaseVoltage">
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Base voltage of the transformer end.  This is essential for PU calculation.</rdfs:comment>
	<rdfs:domain rdf:resource="#TransformerEnd"/>
	<rdfs:range rdf:resource="#BaseVoltage"/>
	<cims:inverseRoleName rdf:resource="#BaseVoltage.TransformerEnds"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BaseVoltage.TransformerEnds">
	<rdfs:label xml:lang="en">TransformerEnds</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Transformer ends at the base voltage.  This is essential for PU calculation.</rdfs:comment>
	<rdfs:domain rdf:resource="#BaseVoltage"/>
	<rdfs:range rdf:resource="#TransformerEnd"/>
	<cims:inverseRoleName rdf:resource="#TransformerEnd.BaseVoltage"/>
	<rdfs:label xml:lang="en">TransformerEnds</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#VoltageLevel.BaseVoltage">
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The base voltage used for all equipment within the voltage level.</rdfs:comment>
	<rdfs:domain rdf:resource="#VoltageLevel"/>
	<rdfs:range rdf:resource="#BaseVoltage"/>
	<cims:inverseRoleName rdf:resource="#BaseVoltage.VoltageLevel"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BaseVoltage.VoltageLevel">
	<rdfs:label xml:lang="en">VoltageLevel</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The voltage levels having this base voltage.</rdfs:comment>
	<rdfs:domain rdf:resource="#BaseVoltage"/>
	<rdfs:range rdf:resource="#VoltageLevel"/>
	<cims:inverseRoleName rdf:resource="#VoltageLevel.BaseVoltage"/>
	<rdfs:label xml:lang="en">VoltageLevel</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BaseVoltage.ConductingEquipment">
	<rdfs:label xml:lang="en">ConductingEquipment</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All conducting equipment with this base voltage.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#BaseVoltage"/>
	<rdfs:range rdf:resource="#ACLineSegment"/>
	<cims:inverseRoleName rdf:resource="#ACLineSegment.BaseVoltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ACLineSegment.BaseVoltage">
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Base voltage of this conducting equipment.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#ACLineSegment"/>
	<rdfs:range rdf:resource="#BaseVoltage"/>
	<cims:inverseRoleName rdf:resource="#BaseVoltage.ConductingEquipment"/>
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BaseVoltage.ConductingEquipment">
	<rdfs:label xml:lang="en">ConductingEquipment</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All conducting equipment with this base voltage.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#BaseVoltage"/>
	<rdfs:range rdf:resource="#Breaker"/>
	<cims:inverseRoleName rdf:resource="#Breaker.BaseVoltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Breaker.BaseVoltage">
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Base voltage of this conducting equipment.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#Breaker"/>
	<rdfs:range rdf:resource="#BaseVoltage"/>
	<cims:inverseRoleName rdf:resource="#BaseVoltage.ConductingEquipment"/>
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BaseVoltage.ConductingEquipment">
	<rdfs:label xml:lang="en">ConductingEquipment</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All conducting equipment with this base voltage.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#BaseVoltage"/>
	<rdfs:range rdf:resource="#Disconnector"/>
	<cims:inverseRoleName rdf:resource="#Disconnector.BaseVoltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Disconnector.BaseVoltage">
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Base voltage of this conducting equipment.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#Disconnector"/>
	<rdfs:range rdf:resource="#BaseVoltage"/>
	<cims:inverseRoleName rdf:resource="#BaseVoltage.ConductingEquipment"/>
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BaseVoltage.ConductingEquipment">
	<rdfs:label xml:lang="en">ConductingEquipment</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All conducting equipment with this base voltage.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#BaseVoltage"/>
	<rdfs:range rdf:resource="#GroundDisconnector"/>
	<cims:inverseRoleName rdf:resource="#GroundDisconnector.BaseVoltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#GroundDisconnector.BaseVoltage">
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Base voltage of this conducting equipment.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#GroundDisconnector"/>
	<rdfs:range rdf:resource="#BaseVoltage"/>
	<cims:inverseRoleName rdf:resource="#BaseVoltage.ConductingEquipment"/>
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BaseVoltage.ConductingEquipment">
	<rdfs:label xml:lang="en">ConductingEquipment</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All conducting equipment with this base voltage.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#BaseVoltage"/>
	<rdfs:range rdf:resource="#LoadBreakSwitch"/>
	<cims:inverseRoleName rdf:resource="#LoadBreakSwitch.BaseVoltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#LoadBreakSwitch.BaseVoltage">
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Base voltage of this conducting equipment.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#LoadBreakSwitch"/>
	<rdfs:range rdf:resource="#BaseVoltage"/>
	<cims:inverseRoleName rdf:resource="#BaseVoltage.ConductingEquipment"/>
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BaseVoltage.ConductingEquipment">
	<rdfs:label xml:lang="en">ConductingEquipment</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All conducting equipment with this base voltage.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#BaseVoltage"/>
	<rdfs:range rdf:resource="#BusbarSection"/>
	<cims:inverseRoleName rdf:resource="#BusbarSection.BaseVoltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BusbarSection.BaseVoltage">
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Base voltage of this conducting equipment.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#BusbarSection"/>
	<rdfs:range rdf:resource="#BaseVoltage"/>
	<cims:inverseRoleName rdf:resource="#BaseVoltage.ConductingEquipment"/>
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BaseVoltage.ConductingEquipment">
	<rdfs:label xml:lang="en">ConductingEquipment</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All conducting equipment with this base voltage.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#BaseVoltage"/>
	<rdfs:range rdf:resource="#EnergyConsumer"/>
	<cims:inverseRoleName rdf:resource="#EnergyConsumer.BaseVoltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EnergyConsumer.BaseVoltage">
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Base voltage of this conducting equipment.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#EnergyConsumer"/>
	<rdfs:range rdf:resource="#BaseVoltage"/>
	<cims:inverseRoleName rdf:resource="#BaseVoltage.ConductingEquipment"/>
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BaseVoltage.ConductingEquipment">
	<rdfs:label xml:lang="en">ConductingEquipment</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All conducting equipment with this base voltage.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#BaseVoltage"/>
	<rdfs:range rdf:resource="#EnergySource"/>
	<cims:inverseRoleName rdf:resource="#EnergySource.BaseVoltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EnergySource.BaseVoltage">
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Base voltage of this conducting equipment.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#EnergySource"/>
	<rdfs:range rdf:resource="#BaseVoltage"/>
	<cims:inverseRoleName rdf:resource="#BaseVoltage.ConductingEquipment"/>
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BaseVoltage.ConductingEquipment">
	<rdfs:label xml:lang="en">ConductingEquipment</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All conducting equipment with this base voltage.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#BaseVoltage"/>
	<rdfs:range rdf:resource="#ExternalNetworkInjection"/>
	<cims:inverseRoleName rdf:resource="#ExternalNetworkInjection.BaseVoltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ExternalNetworkInjection.BaseVoltage">
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Base voltage of this conducting equipment.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#ExternalNetworkInjection"/>
	<rdfs:range rdf:resource="#BaseVoltage"/>
	<cims:inverseRoleName rdf:resource="#BaseVoltage.ConductingEquipment"/>
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BaseVoltage.ConductingEquipment">
	<rdfs:label xml:lang="en">ConductingEquipment</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All conducting equipment with this base voltage.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#BaseVoltage"/>
	<rdfs:range rdf:resource="#Fuse"/>
	<cims:inverseRoleName rdf:resource="#Fuse.BaseVoltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Fuse.BaseVoltage">
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Base voltage of this conducting equipment.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#Fuse"/>
	<rdfs:range rdf:resource="#BaseVoltage"/>
	<cims:inverseRoleName rdf:resource="#BaseVoltage.ConductingEquipment"/>
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BaseVoltage.ConductingEquipment">
	<rdfs:label xml:lang="en">ConductingEquipment</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All conducting equipment with this base voltage.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#BaseVoltage"/>
	<rdfs:range rdf:resource="#LinearShuntCompensator"/>
	<cims:inverseRoleName rdf:resource="#LinearShuntCompensator.BaseVoltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#LinearShuntCompensator.BaseVoltage">
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Base voltage of this conducting equipment.  Use only when there is no voltage level container used and only one base voltage applies.  For example, not used for transformers.</rdfs:comment>
	<rdfs:domain rdf:resource="#LinearShuntCompensator"/>
	<rdfs:range rdf:resource="#BaseVoltage"/>
	<cims:inverseRoleName rdf:resource="#BaseVoltage.ConductingEquipment"/>
	<rdfs:label xml:lang="en">BaseVoltage</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BasicIntervalSchedule">
	<rdfs:label xml:lang="en">BasicIntervalSchedule</rdfs:label>
	<rdfs:subClassOf rdf:resource="#IdentifiedObject"/>
	<rdfs:comment  rdf:parseType="Literal">Schedule of values at points in time.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BasicIntervalSchedule.startTime">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">startTime</rdfs:label>
	<rdfs:domain rdf:resource="#BasicIntervalSchedule"/>
	<cims:dataType rdf:resource="#DateTime"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The time for the first time point.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#DateTime">
	<rdfs:label xml:lang="en">DateTime</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Date and time as "yyyy-mm-ddThh:mm:ss.sss", which conforms with ISO 8601. UTC time zone is specified as "yyyy-mm-ddThh:mm:ss.sssZ". A local timezone relative UTC is specified as "yyyy-mm-ddThh:mm:ss.sss-hh:mm". The second component (shown here as "ss.sss") could have any number of digits in its fractional part to allow any kind of precision beyond seconds.</rdfs:comment>
<cims:stereotype>Primitive</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BasicIntervalSchedule.value1Multiplier">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">value1Multiplier</rdfs:label>
	<rdfs:domain rdf:resource="#BasicIntervalSchedule"/>
	<cims:dataType rdf:resource="#UnitMultiplier"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Multiplier for value1.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BasicIntervalSchedule.value1Unit">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">value1Unit</rdfs:label>
	<rdfs:domain rdf:resource="#BasicIntervalSchedule"/>
	<cims:dataType rdf:resource="#UnitSymbol"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Value1 units of measure.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BasicIntervalSchedule.value2Multiplier">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">value2Multiplier</rdfs:label>
	<rdfs:domain rdf:resource="#BasicIntervalSchedule"/>
	<cims:dataType rdf:resource="#UnitMultiplier"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Multiplier for value2.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BasicIntervalSchedule.value2Unit">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">value2Unit</rdfs:label>
	<rdfs:domain rdf:resource="#BasicIntervalSchedule"/>
	<cims:dataType rdf:resource="#UnitSymbol"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Value2 units of measure.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BasicIntervalSchedule.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#BasicIntervalSchedule"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BasicIntervalSchedule.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#BasicIntervalSchedule"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Bay">
	<rdfs:label xml:lang="en">Bay</rdfs:label>
	<rdfs:subClassOf rdf:resource="#EquipmentContainer"/>
	<rdfs:comment  rdf:parseType="Literal">A collection of power system resources (within a given substation) including conducting equipment, protection relays, measurements, and telemetry.  A bay typically represents a physical grouping related to modularization of equipment.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#VoltageLevel.Bays">
	<rdfs:label xml:lang="en">Bays</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The bays within this voltage level.</rdfs:comment>
	<rdfs:domain rdf:resource="#VoltageLevel"/>
	<rdfs:range rdf:resource="#Bay"/>
	<cims:inverseRoleName rdf:resource="#Bay.VoltageLevel"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Bay.VoltageLevel">
	<rdfs:label xml:lang="en">VoltageLevel</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The voltage level containing this bay.</rdfs:comment>
	<rdfs:domain rdf:resource="#Bay"/>
	<rdfs:range rdf:resource="#VoltageLevel"/>
	<cims:inverseRoleName rdf:resource="#VoltageLevel.Bays"/>
	<rdfs:label xml:lang="en">VoltageLevel</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConductingEquipment">
	<rdfs:label xml:lang="en">ConductingEquipment</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The parts of the AC power system that are designed to carry current or that are conductively connected through terminals.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Terminal.ConductingEquipment">
	<rdfs:label xml:lang="en">ConductingEquipment</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The conducting equipment of the terminal.  Conducting equipment have  terminals that may be connected to other conducting equipment terminals via connectivity nodes or topological nodes.</rdfs:comment>
	<rdfs:domain rdf:resource="#Terminal"/>
	<rdfs:range rdf:resource="#ConductingEquipment"/>
	<cims:inverseRoleName rdf:resource="#ConductingEquipment.Terminals"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConductingEquipment.Terminals">
	<rdfs:label xml:lang="en">Terminals</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Conducting equipment have terminals that may be connected to other conducting equipment terminals via connectivity nodes or topological nodes.</rdfs:comment>
	<rdfs:domain rdf:resource="#ConductingEquipment"/>
	<rdfs:range rdf:resource="#Terminal"/>
	<cims:inverseRoleName rdf:resource="#Terminal.ConductingEquipment"/>
	<rdfs:label xml:lang="en">Terminals</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConnectivityNode">
	<rdfs:label xml:lang="en">ConnectivityNode</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Connectivity nodes are points where terminals of AC conducting equipment are connected together with zero impedance.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConnectivityNode.ConnectivityNodeContainer">
	<rdfs:label xml:lang="en">ConnectivityNodeContainer</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Container of this connectivity node.</rdfs:comment>
	<rdfs:domain rdf:resource="#ConnectivityNode"/>
	<rdfs:range rdf:resource="#ConnectivityNodeContainer"/>
	<cims:inverseRoleName rdf:resource="#ConnectivityNodeContainer.ConnectivityNodes"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConnectivityNodeContainer.ConnectivityNodes">
	<rdfs:label xml:lang="en">ConnectivityNodes</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Connectivity nodes which belong to this connectivity node container.</rdfs:comment>
	<rdfs:domain rdf:resource="#ConnectivityNodeContainer"/>
	<rdfs:range rdf:resource="#ConnectivityNode"/>
	<cims:inverseRoleName rdf:resource="#ConnectivityNode.ConnectivityNodeContainer"/>
	<rdfs:label xml:lang="en">ConnectivityNodes</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Terminal.ConnectivityNode">
	<rdfs:label xml:lang="en">ConnectivityNode</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The connectivity node to which this terminal connects with zero impedance.</rdfs:comment>
	<rdfs:domain rdf:resource="#Terminal"/>
	<rdfs:range rdf:resource="#ConnectivityNode"/>
	<cims:inverseRoleName rdf:resource="#ConnectivityNode.Terminals"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConnectivityNode.Terminals">
	<rdfs:label xml:lang="en">Terminals</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Terminals interconnected with zero impedance at a this connectivity node. </rdfs:comment>
	<rdfs:domain rdf:resource="#ConnectivityNode"/>
	<rdfs:range rdf:resource="#Terminal"/>
	<cims:inverseRoleName rdf:resource="#Terminal.ConnectivityNode"/>
	<rdfs:label xml:lang="en">Terminals</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConnectivityNodeContainer">
	<rdfs:label xml:lang="en">ConnectivityNodeContainer</rdfs:label>
	<rdfs:subClassOf rdf:resource="#PowerSystemResource"/>
	<rdfs:comment  rdf:parseType="Literal">A base class for all objects that may contain connectivity nodes or topological nodes.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Equipment">
	<rdfs:label xml:lang="en">Equipment</rdfs:label>
	<rdfs:subClassOf rdf:resource="#PowerSystemResource"/>
	<rdfs:comment  rdf:parseType="Literal">The parts of a power system that are physical devices, electronic or mechanical.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Equipment.aggregate">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">aggregate</rdfs:label>
	<rdfs:domain rdf:resource="#Equipment"/>
	<cims:dataType rdf:resource="#Boolean"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The single instance of equipment represents multiple pieces of equipment that have been modeled together as an aggregate.  Examples would be power transformers or synchronous machines operating in parallel modeled as a single aggregate power transformer or aggregate synchronous machine.  This is not to be used to indicate equipment that is part of a group of interdependent equipment produced by a network production program.  </rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Equipment.inService">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">inService</rdfs:label>
	<rdfs:domain rdf:resource="#Equipment"/>
	<cims:dataType rdf:resource="#Boolean"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">If true, the equipment is in service.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Equipment.normallyInService">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">normallyInService</rdfs:label>
	<rdfs:domain rdf:resource="#Equipment"/>
	<cims:dataType rdf:resource="#Boolean"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">If true, the equipment is normally in service.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EquipmentContainer.Equipments">
	<rdfs:label xml:lang="en">Equipments</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Contained equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#EquipmentContainer"/>
	<rdfs:range rdf:resource="#Equipment"/>
	<cims:inverseRoleName rdf:resource="#Equipment.EquipmentContainer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Equipment.EquipmentContainer">
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Container of this equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#Equipment"/>
	<rdfs:range rdf:resource="#EquipmentContainer"/>
	<cims:inverseRoleName rdf:resource="#EquipmentContainer.Equipments"/>
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EquipmentContainer">
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<rdfs:subClassOf rdf:resource="#ConnectivityNodeContainer"/>
	<rdfs:comment  rdf:parseType="Literal">A modeling construct to provide a root class for containing equipment. </rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EquipmentContainer.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#EquipmentContainer"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EquipmentContainer.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#EquipmentContainer"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EquipmentContainer.Equipments">
	<rdfs:label xml:lang="en">Equipments</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Contained equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#EquipmentContainer"/>
	<rdfs:range rdf:resource="#PowerTransformer"/>
	<cims:inverseRoleName rdf:resource="#PowerTransformer.EquipmentContainer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PowerTransformer.EquipmentContainer">
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Container of this equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#PowerTransformer"/>
	<rdfs:range rdf:resource="#EquipmentContainer"/>
	<cims:inverseRoleName rdf:resource="#EquipmentContainer.Equipments"/>
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EquipmentContainer.Equipments">
	<rdfs:label xml:lang="en">Equipments</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Contained equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#EquipmentContainer"/>
	<rdfs:range rdf:resource="#ACLineSegment"/>
	<cims:inverseRoleName rdf:resource="#ACLineSegment.EquipmentContainer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ACLineSegment.EquipmentContainer">
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Container of this equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#ACLineSegment"/>
	<rdfs:range rdf:resource="#EquipmentContainer"/>
	<cims:inverseRoleName rdf:resource="#EquipmentContainer.Equipments"/>
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EquipmentContainer.Equipments">
	<rdfs:label xml:lang="en">Equipments</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Contained equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#EquipmentContainer"/>
	<rdfs:range rdf:resource="#Breaker"/>
	<cims:inverseRoleName rdf:resource="#Breaker.EquipmentContainer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Breaker.EquipmentContainer">
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Container of this equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#Breaker"/>
	<rdfs:range rdf:resource="#EquipmentContainer"/>
	<cims:inverseRoleName rdf:resource="#EquipmentContainer.Equipments"/>
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EquipmentContainer.Equipments">
	<rdfs:label xml:lang="en">Equipments</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Contained equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#EquipmentContainer"/>
	<rdfs:range rdf:resource="#Disconnector"/>
	<cims:inverseRoleName rdf:resource="#Disconnector.EquipmentContainer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Disconnector.EquipmentContainer">
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Container of this equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#Disconnector"/>
	<rdfs:range rdf:resource="#EquipmentContainer"/>
	<cims:inverseRoleName rdf:resource="#EquipmentContainer.Equipments"/>
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EquipmentContainer.Equipments">
	<rdfs:label xml:lang="en">Equipments</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Contained equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#EquipmentContainer"/>
	<rdfs:range rdf:resource="#GroundDisconnector"/>
	<cims:inverseRoleName rdf:resource="#GroundDisconnector.EquipmentContainer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#GroundDisconnector.EquipmentContainer">
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Container of this equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#GroundDisconnector"/>
	<rdfs:range rdf:resource="#EquipmentContainer"/>
	<cims:inverseRoleName rdf:resource="#EquipmentContainer.Equipments"/>
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EquipmentContainer.Equipments">
	<rdfs:label xml:lang="en">Equipments</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Contained equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#EquipmentContainer"/>
	<rdfs:range rdf:resource="#LoadBreakSwitch"/>
	<cims:inverseRoleName rdf:resource="#LoadBreakSwitch.EquipmentContainer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#LoadBreakSwitch.EquipmentContainer">
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Container of this equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#LoadBreakSwitch"/>
	<rdfs:range rdf:resource="#EquipmentContainer"/>
	<cims:inverseRoleName rdf:resource="#EquipmentContainer.Equipments"/>
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EquipmentContainer.Equipments">
	<rdfs:label xml:lang="en">Equipments</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Contained equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#EquipmentContainer"/>
	<rdfs:range rdf:resource="#BusbarSection"/>
	<cims:inverseRoleName rdf:resource="#BusbarSection.EquipmentContainer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BusbarSection.EquipmentContainer">
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Container of this equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#BusbarSection"/>
	<rdfs:range rdf:resource="#EquipmentContainer"/>
	<cims:inverseRoleName rdf:resource="#EquipmentContainer.Equipments"/>
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EquipmentContainer.Equipments">
	<rdfs:label xml:lang="en">Equipments</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Contained equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#EquipmentContainer"/>
	<rdfs:range rdf:resource="#EnergyConsumer"/>
	<cims:inverseRoleName rdf:resource="#EnergyConsumer.EquipmentContainer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EnergyConsumer.EquipmentContainer">
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Container of this equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#EnergyConsumer"/>
	<rdfs:range rdf:resource="#EquipmentContainer"/>
	<cims:inverseRoleName rdf:resource="#EquipmentContainer.Equipments"/>
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EquipmentContainer.Equipments">
	<rdfs:label xml:lang="en">Equipments</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Contained equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#EquipmentContainer"/>
	<rdfs:range rdf:resource="#EnergySource"/>
	<cims:inverseRoleName rdf:resource="#EnergySource.EquipmentContainer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EnergySource.EquipmentContainer">
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Container of this equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#EnergySource"/>
	<rdfs:range rdf:resource="#EquipmentContainer"/>
	<cims:inverseRoleName rdf:resource="#EquipmentContainer.Equipments"/>
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EquipmentContainer.Equipments">
	<rdfs:label xml:lang="en">Equipments</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Contained equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#EquipmentContainer"/>
	<rdfs:range rdf:resource="#ExternalNetworkInjection"/>
	<cims:inverseRoleName rdf:resource="#ExternalNetworkInjection.EquipmentContainer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ExternalNetworkInjection.EquipmentContainer">
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Container of this equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#ExternalNetworkInjection"/>
	<rdfs:range rdf:resource="#EquipmentContainer"/>
	<cims:inverseRoleName rdf:resource="#EquipmentContainer.Equipments"/>
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EquipmentContainer.Equipments">
	<rdfs:label xml:lang="en">Equipments</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Contained equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#EquipmentContainer"/>
	<rdfs:range rdf:resource="#Fuse"/>
	<cims:inverseRoleName rdf:resource="#Fuse.EquipmentContainer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Fuse.EquipmentContainer">
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Container of this equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#Fuse"/>
	<rdfs:range rdf:resource="#EquipmentContainer"/>
	<cims:inverseRoleName rdf:resource="#EquipmentContainer.Equipments"/>
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EquipmentContainer.Equipments">
	<rdfs:label xml:lang="en">Equipments</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Contained equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#EquipmentContainer"/>
	<rdfs:range rdf:resource="#LinearShuntCompensator"/>
	<cims:inverseRoleName rdf:resource="#LinearShuntCompensator.EquipmentContainer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#LinearShuntCompensator.EquipmentContainer">
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Container of this equipment.</rdfs:comment>
	<rdfs:domain rdf:resource="#LinearShuntCompensator"/>
	<rdfs:range rdf:resource="#EquipmentContainer"/>
	<cims:inverseRoleName rdf:resource="#EquipmentContainer.Equipments"/>
	<rdfs:label xml:lang="en">EquipmentContainer</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#GeographicalRegion">
	<rdfs:label xml:lang="en">GeographicalRegion</rdfs:label>
	<rdfs:subClassOf rdf:resource="#IdentifiedObject"/>
	<rdfs:comment  rdf:parseType="Literal">A geographical region of a power system network model.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#GeographicalRegion.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#GeographicalRegion"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#GeographicalRegion.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#GeographicalRegion"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#GeographicalRegion.Regions">
	<rdfs:label xml:lang="en">Regions</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All sub-geograhpical regions within this geographical region.</rdfs:comment>
	<rdfs:domain rdf:resource="#GeographicalRegion"/>
	<rdfs:range rdf:resource="#SubGeographicalRegion"/>
	<cims:inverseRoleName rdf:resource="#SubGeographicalRegion.Region"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#SubGeographicalRegion.Region">
	<rdfs:label xml:lang="en">Region</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The geographical region to which this sub-geographical region is within.</rdfs:comment>
	<rdfs:domain rdf:resource="#SubGeographicalRegion"/>
	<rdfs:range rdf:resource="#GeographicalRegion"/>
	<cims:inverseRoleName rdf:resource="#GeographicalRegion.Regions"/>
	<rdfs:label xml:lang="en">Region</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">This is a root class to provide common identification for all classes needing identification and naming attributes.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#IdentifiedObject.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#IdentifiedObject"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#IdentifiedObject.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#IdentifiedObject"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#IdentifiedObject.description">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">description</rdfs:label>
	<rdfs:domain rdf:resource="#IdentifiedObject"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The description is a free human readable text describing or naming the object. It may be non unique and may not correlate to a naming hierarchy.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#IdentifiedObject"/>
	<cims:inverseRoleName rdf:resource="#IdentifiedObject.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#IdentifiedObject.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#IdentifiedObject"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name">
	<rdfs:label xml:lang="en">Name</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The Name class provides the means to define any number of human readable  names for an object. A name is <b>not</b> to be used for defining inter-object relationships. For inter-object relationships instead use the object identification 'mRID'.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#Name"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Any free text that name the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#ConformLoadSchedule"/>
	<cims:inverseRoleName rdf:resource="#ConformLoadSchedule.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConformLoadSchedule.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#ConformLoadSchedule"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#PowerTransformer"/>
	<cims:inverseRoleName rdf:resource="#PowerTransformer.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PowerTransformer.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#PowerTransformer"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#RatioTapChanger"/>
	<cims:inverseRoleName rdf:resource="#RatioTapChanger.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RatioTapChanger.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#RatioTapChanger"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#RegulatingControl"/>
	<cims:inverseRoleName rdf:resource="#RegulatingControl.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RegulatingControl.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#RegulatingControl"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#PerLengthImpedance"/>
	<cims:inverseRoleName rdf:resource="#PerLengthImpedance.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PerLengthImpedance.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#PerLengthImpedance"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#Breaker"/>
	<cims:inverseRoleName rdf:resource="#Breaker.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Breaker.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#Breaker"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#Disconnector"/>
	<cims:inverseRoleName rdf:resource="#Disconnector.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Disconnector.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#Disconnector"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#GroundDisconnector"/>
	<cims:inverseRoleName rdf:resource="#GroundDisconnector.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#GroundDisconnector.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#GroundDisconnector"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#BusbarSection"/>
	<cims:inverseRoleName rdf:resource="#BusbarSection.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BusbarSection.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#BusbarSection"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#EnergyConsumer"/>
	<cims:inverseRoleName rdf:resource="#EnergyConsumer.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EnergyConsumer.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#EnergyConsumer"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#EnergySource"/>
	<cims:inverseRoleName rdf:resource="#EnergySource.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EnergySource.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#EnergySource"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#ExternalNetworkInjection"/>
	<cims:inverseRoleName rdf:resource="#ExternalNetworkInjection.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ExternalNetworkInjection.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#ExternalNetworkInjection"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#RegulatingCondEq"/>
	<cims:inverseRoleName rdf:resource="#RegulatingCondEq.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RegulatingCondEq.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#RegulatingCondEq"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#Fuse"/>
	<cims:inverseRoleName rdf:resource="#Fuse.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Fuse.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#Fuse"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#Line"/>
	<cims:inverseRoleName rdf:resource="#Line.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Line.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#Line"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Name.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Identified object that this name designates.</rdfs:comment>
	<rdfs:domain rdf:resource="#Name"/>
	<rdfs:range rdf:resource="#LinearShuntCompensator"/>
	<cims:inverseRoleName rdf:resource="#LinearShuntCompensator.Names"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#LinearShuntCompensator.Names">
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All names of this identified object.</rdfs:comment>
	<rdfs:domain rdf:resource="#LinearShuntCompensator"/>
	<rdfs:range rdf:resource="#Name"/>
	<cims:inverseRoleName rdf:resource="#Name.IdentifiedObject"/>
	<rdfs:label xml:lang="en">Names</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode">
	<rdfs:label xml:lang="en">PhaseCode</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">An unordered enumeration of phase identifiers.  Allows designation of phases for both transmission and distribution equipment, circuits and loads.   The enumeration, by itself, does not describe how the phases are connected together or connected to ground.  Ground is not explicitly denoted as a phase.
Residential and small commercial loads are often served from single-phase, or split-phase, secondary circuits. For example of s12N, phases 1 and 2 refer to hot wires that are 180 degrees out of phase, while N refers to the neutral wire. Through single-phase transformer connections, these secondary circuits may be served from one or two of the primary phases A, B, and C. For three-phase loads, use the A, B, C phase codes instead of s12N.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.ABCN">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ABCN</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="225" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Phases A, B, C, and N.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.ABC">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ABC</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="224" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Phases A, B, and C.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.ABN">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ABN</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="193" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Phases A, B, and neutral.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.ACN">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ACN</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="41" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Phases A, C and neutral.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.BCN">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">BCN</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="97" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Phases B, C, and neutral.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.AB">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">AB</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="132" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Phases A and B.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.s1N">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">s1N</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="528" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Secondary phase 1 and neutral.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.s12N">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">s12N</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="784" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Secondary phases 1, 2, and neutral.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.s1">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">s1</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="512" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Secondary phase 1.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.s2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">s2</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="256" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Secondary phase 2.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.s12">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">s12</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="768" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Secondary phase 1 and 2.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.none">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">none</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="0" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">No phases specified.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.X">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">X</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="1024" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Unknown non-neutral phase.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.XY">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">XY</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="3072" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Two unknown non-neutral phases.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.XN">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">XN</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="1040" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Unknown non-neutral phase plus neutral.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.XYN">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">XYN</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="3088" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Two unknown non-neutral phases plus neutral.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.AC">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">AC</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="96" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Phases A and C.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.BC">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">BC</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="66" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Phases B and C.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.AN">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">AN</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="129" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Phases A and neutral.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.CN">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">CN</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="33" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Phases C and neutral.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.BN">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">BN</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="65" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Phases B and neutral.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.B">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">B</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="64" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Phase B.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.C">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">C</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="32" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Phase C.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.A">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">A</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="128" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Phase A.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.N">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">N</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="16" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Neutral phase.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseCode.s2N">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">s2N</rdfs:label>
	<rdfs:domain rdf:resource="#PhaseCode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="272" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Secondary phase 2 and neutral.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PowerSystemResource">
	<rdfs:label xml:lang="en">PowerSystemResource</rdfs:label>
	<rdfs:subClassOf rdf:resource="#IdentifiedObject"/>
	<rdfs:comment  rdf:parseType="Literal">A power system resource can be an item of equipment such as a switch, an equipment container containing many individual items of equipment such as a substation, or an organisational entity such as sub-control area. Power system resources can have measurements associated.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PowerSystemResource.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#PowerSystemResource"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PowerSystemResource.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#PowerSystemResource"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RegularIntervalSchedule">
	<rdfs:label xml:lang="en">RegularIntervalSchedule</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The schedule has time points where the time between them is constant.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RegularIntervalSchedule.TimePoints">
	<rdfs:label xml:lang="en">TimePoints</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The regular interval time point data values that define this schedule.</rdfs:comment>
	<rdfs:domain rdf:resource="#RegularIntervalSchedule"/>
	<rdfs:range rdf:resource="#RegularTimePoint"/>
	<cims:inverseRoleName rdf:resource="#RegularTimePoint.IntervalSchedule"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RegularTimePoint.IntervalSchedule">
	<rdfs:label xml:lang="en">IntervalSchedule</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Regular interval schedule containing this time point.</rdfs:comment>
	<rdfs:domain rdf:resource="#RegularTimePoint"/>
	<rdfs:range rdf:resource="#RegularIntervalSchedule"/>
	<cims:inverseRoleName rdf:resource="#RegularIntervalSchedule.TimePoints"/>
	<rdfs:label xml:lang="en">IntervalSchedule</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RegularTimePoint">
	<rdfs:label xml:lang="en">RegularTimePoint</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Time point for a schedule where the time between the consecutive points is constant.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RegularTimePoint.sequenceNumber">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">sequenceNumber</rdfs:label>
	<rdfs:domain rdf:resource="#RegularTimePoint"/>
	<cims:dataType rdf:resource="#Integer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The position of the regular time point in the sequence. Note that time points don't have to be sequential, i.e. time points may be omitted. The actual time for a RegularTimePoint is computed by multiplying the associated regular interval schedule's time step with the regular time point sequence number and adding the associated schedules start time.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RegularTimePoint.value1">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">value1</rdfs:label>
	<rdfs:domain rdf:resource="#RegularTimePoint"/>
	<cims:dataType rdf:resource="#Float"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The first value at the time. The meaning of the value is defined by the derived type of the associated schedule.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RegularTimePoint.value2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">value2</rdfs:label>
	<rdfs:domain rdf:resource="#RegularTimePoint"/>
	<cims:dataType rdf:resource="#Float"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The second value at the time. The meaning of the value is defined by the derived type of the associated schedule.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConformLoadSchedule.TimePoints">
	<rdfs:label xml:lang="en">TimePoints</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The regular interval time point data values that define this schedule.</rdfs:comment>
	<rdfs:domain rdf:resource="#ConformLoadSchedule"/>
	<rdfs:range rdf:resource="#RegularTimePoint"/>
	<cims:inverseRoleName rdf:resource="#RegularTimePoint.IntervalSchedule"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RegularTimePoint.IntervalSchedule">
	<rdfs:label xml:lang="en">IntervalSchedule</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Regular interval schedule containing this time point.</rdfs:comment>
	<rdfs:domain rdf:resource="#RegularTimePoint"/>
	<rdfs:range rdf:resource="#ConformLoadSchedule"/>
	<cims:inverseRoleName rdf:resource="#ConformLoadSchedule.TimePoints"/>
	<rdfs:label xml:lang="en">IntervalSchedule</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#SubGeographicalRegion">
	<rdfs:label xml:lang="en">SubGeographicalRegion</rdfs:label>
	<rdfs:subClassOf rdf:resource="#IdentifiedObject"/>
	<rdfs:comment  rdf:parseType="Literal">A subset of a geographical region of a power system network model.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#SubGeographicalRegion.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#SubGeographicalRegion"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#SubGeographicalRegion.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#SubGeographicalRegion"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#SubGeographicalRegion.Substations">
	<rdfs:label xml:lang="en">Substations</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The substations in this sub-geographical region.</rdfs:comment>
	<rdfs:domain rdf:resource="#SubGeographicalRegion"/>
	<rdfs:range rdf:resource="#Substation"/>
	<cims:inverseRoleName rdf:resource="#Substation.Region"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Substation.Region">
	<rdfs:label xml:lang="en">Region</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The SubGeographicalRegion containing the substation.</rdfs:comment>
	<rdfs:domain rdf:resource="#Substation"/>
	<rdfs:range rdf:resource="#SubGeographicalRegion"/>
	<cims:inverseRoleName rdf:resource="#SubGeographicalRegion.Substations"/>
	<rdfs:label xml:lang="en">Region</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#SubGeographicalRegion.Lines">
	<rdfs:label xml:lang="en">Lines</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The lines within the sub-geographical region.</rdfs:comment>
	<rdfs:domain rdf:resource="#SubGeographicalRegion"/>
	<rdfs:range rdf:resource="#Line"/>
	<cims:inverseRoleName rdf:resource="#Line.Region"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Line.Region">
	<rdfs:label xml:lang="en">Region</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The sub-geographical region of the line.</rdfs:comment>
	<rdfs:domain rdf:resource="#Line"/>
	<rdfs:range rdf:resource="#SubGeographicalRegion"/>
	<cims:inverseRoleName rdf:resource="#SubGeographicalRegion.Lines"/>
	<rdfs:label xml:lang="en">Region</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Substation">
	<rdfs:label xml:lang="en">Substation</rdfs:label>
	<rdfs:subClassOf rdf:resource="#EquipmentContainer"/>
	<rdfs:comment  rdf:parseType="Literal">A collection of equipment for purposes other than generation or utilization, through which electric energy in bulk is passed for the purposes of switching or modifying its characteristics. </rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Substation.VoltageLevels">
	<rdfs:label xml:lang="en">VoltageLevels</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The voltage levels within this substation.</rdfs:comment>
	<rdfs:domain rdf:resource="#Substation"/>
	<rdfs:range rdf:resource="#VoltageLevel"/>
	<cims:inverseRoleName rdf:resource="#VoltageLevel.Substation"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#VoltageLevel.Substation">
	<rdfs:label xml:lang="en">Substation</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The substation of the voltage level.</rdfs:comment>
	<rdfs:domain rdf:resource="#VoltageLevel"/>
	<rdfs:range rdf:resource="#Substation"/>
	<cims:inverseRoleName rdf:resource="#Substation.VoltageLevels"/>
	<rdfs:label xml:lang="en">Substation</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Terminal">
	<rdfs:label xml:lang="en">Terminal</rdfs:label>
	<rdfs:subClassOf rdf:resource="#IdentifiedObject"/>
	<rdfs:comment  rdf:parseType="Literal">An AC electrical connection point to a piece of conducting equipment. Terminals are connected at physical connection points called connectivity nodes.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Terminal.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#Terminal"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Terminal.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#Terminal"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Terminal.connected">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">connected</rdfs:label>
	<rdfs:domain rdf:resource="#Terminal"/>
	<cims:dataType rdf:resource="#Boolean"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The connected status is related to a bus-branch model and the topological node to terminal relation.  True implies the terminal is connected to the related topological node and false implies it is not. 
In a bus-branch model, the connected status is used to tell if equipment is disconnected without having to change the connectivity described by the topological node to terminal relation. A valid case is that conducting equipment can be connected in one end and open in the other. In particular for an AC line segment, where the reactive line charging can be significant, this is a relevant case.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#TransformerEnd.Terminal">
	<rdfs:label xml:lang="en">Terminal</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Terminal of the power transformer to which this transformer end belongs.</rdfs:comment>
	<rdfs:domain rdf:resource="#TransformerEnd"/>
	<rdfs:range rdf:resource="#Terminal"/>
	<cims:inverseRoleName rdf:resource="#Terminal.TransformerEnd"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Terminal.TransformerEnd">
	<rdfs:label xml:lang="en">TransformerEnd</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All transformer ends connected at this terminal.</rdfs:comment>
	<rdfs:domain rdf:resource="#Terminal"/>
	<rdfs:range rdf:resource="#TransformerEnd"/>
	<cims:inverseRoleName rdf:resource="#TransformerEnd.Terminal"/>
	<rdfs:label xml:lang="en">TransformerEnd</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Terminal.RegulatingControl">
	<rdfs:label xml:lang="en">RegulatingControl</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The controls regulating this terminal.</rdfs:comment>
	<rdfs:domain rdf:resource="#Terminal"/>
	<rdfs:range rdf:resource="#RegulatingControl"/>
	<cims:inverseRoleName rdf:resource="#RegulatingControl.Terminal"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RegulatingControl.Terminal">
	<rdfs:label xml:lang="en">Terminal</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The terminal associated with this regulating control.  The terminal is associated instead of a node, since the terminal could connect into either a topological node (bus in bus-branch model) or a connectivity node (detailed switch model).  Sometimes it is useful to model regulation at a terminal of a bus bar object since the bus bar can be present in both a bus-branch model or a model with switch detail.</rdfs:comment>
	<rdfs:domain rdf:resource="#RegulatingControl"/>
	<rdfs:range rdf:resource="#Terminal"/>
	<cims:inverseRoleName rdf:resource="#Terminal.RegulatingControl"/>
	<rdfs:label xml:lang="en">Terminal</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#VoltageLevel">
	<rdfs:label xml:lang="en">VoltageLevel</rdfs:label>
	<rdfs:subClassOf rdf:resource="#EquipmentContainer"/>
	<rdfs:comment  rdf:parseType="Literal">A collection of equipment at one common system voltage forming a switchgear. The equipment typically consist of breakers, busbars, instrumentation, control, regulation and protection devices as well as assemblies of all these.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Core"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#VoltageLevel.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#VoltageLevel"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#VoltageLevel.highVoltageLimit">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">highVoltageLimit</rdfs:label>
	<rdfs:domain rdf:resource="#VoltageLevel"/>
	<cims:dataType rdf:resource="#Voltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The bus bar's high voltage limit</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#VoltageLevel.lowVoltageLimit">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">lowVoltageLimit</rdfs:label>
	<rdfs:domain rdf:resource="#VoltageLevel"/>
	<cims:dataType rdf:resource="#Voltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The bus bar's low voltage limit</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#VoltageLevel.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#VoltageLevel"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_DC">
	<rdfs:label xml:lang="en">DC</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">This package contains model for direct current equipment and controls.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#ACDCConverter">
	<rdfs:label xml:lang="en">ACDCConverter</rdfs:label>
	<rdfs:subClassOf rdf:resource="#PowerSystemResource"/>
	<rdfs:comment  rdf:parseType="Literal">A unit with valves for three phases, together with unit control equipment, essential protective and switching devices, DC storage capacitors, phase reactors and auxiliaries, if any, used for conversion.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_DC"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ACDCConverter.p">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">p</rdfs:label>
	<rdfs:domain rdf:resource="#ACDCConverter"/>
	<cims:dataType rdf:resource="#ActivePower"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Active power at the point of common coupling. Load sign convention is used, i.e. positive sign means flow out from a node.
Starting value for a steady state solution in the case a simplified power flow model is used.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ActivePower">
	<rdfs:label xml:lang="en">ActivePower</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Product of RMS value of the voltage and the RMS value of the in-phase component of the current.</rdfs:comment>
<cims:stereotype>CIMDatatype</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ActivePower.multiplier">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">multiplier</rdfs:label>
	<rdfs:domain rdf:resource="#ActivePower"/>
	<cims:dataType rdf:resource="#UnitMultiplier"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ACDCConverter.q">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">q</rdfs:label>
	<rdfs:domain rdf:resource="#ACDCConverter"/>
	<cims:dataType rdf:resource="#ReactivePower"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Reactive power at the point of common coupling. Load sign convention is used, i.e. positive sign means flow out from a node.
Starting value for a steady state solution in the case a simplified power flow model is used.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ReactivePower">
	<rdfs:label xml:lang="en">ReactivePower</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Product of RMS value of the voltage and the RMS value of the quadrature component of the current.</rdfs:comment>
<cims:stereotype>CIMDatatype</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ReactivePower.multiplier">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">multiplier</rdfs:label>
	<rdfs:domain rdf:resource="#ReactivePower"/>
	<cims:dataType rdf:resource="#UnitMultiplier"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ReactivePower.unit">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">unit</rdfs:label>
	<rdfs:domain rdf:resource="#ReactivePower"/>
	<cims:dataType rdf:resource="#UnitSymbol"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<cims:isFixed rdfs:Literal="VAr" />
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ReactivePower.value">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">value</rdfs:label>
	<rdfs:domain rdf:resource="#ReactivePower"/>
	<cims:dataType rdf:resource="#Float"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ACDCConverter.uc">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">uc</rdfs:label>
	<rdfs:domain rdf:resource="#ACDCConverter"/>
	<cims:dataType rdf:resource="#Voltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Line-to-line converter voltage, the voltage at the AC side of the valve. Converter state variable, result from power flow.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ACDCConverter.udc">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">udc</rdfs:label>
	<rdfs:domain rdf:resource="#ACDCConverter"/>
	<cims:dataType rdf:resource="#Voltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Converter voltage at the DC side, also called Ud. Converter state variable, result from power flow.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ACDCConverter.idc">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">idc</rdfs:label>
	<rdfs:domain rdf:resource="#ACDCConverter"/>
	<cims:dataType rdf:resource="#CurrentFlow"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Converter DC current, also called Id. Converter state variable, result from power flow.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ACDCConverter.ratedUdc">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ratedUdc</rdfs:label>
	<rdfs:domain rdf:resource="#ACDCConverter"/>
	<cims:dataType rdf:resource="#Voltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Rated converter DC voltage, also called UdN. Converter configuration data used in power flow.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_DiagramLayout">
	<rdfs:label xml:lang="en">DiagramLayout</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">This package describes diagram layout. This describes how objects are arranged in a coordianate system rather than how they are rendered.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#Diagram">
	<rdfs:label xml:lang="en">Diagram</rdfs:label>
	<rdfs:subClassOf rdf:resource="#IdentifiedObject"/>
	<rdfs:comment  rdf:parseType="Literal">The diagram being exchanged.  The coordinate system is a standard Cartesian coordinate system and the orientation attribute defines the orientation.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_DiagramLayout"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Diagram.orientation">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">orientation</rdfs:label>
	<rdfs:domain rdf:resource="#Diagram"/>
	<cims:dataType rdf:resource="#OrientationKind"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Coordinate system orientation of the diagram.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#OrientationKind">
	<rdfs:label xml:lang="en">OrientationKind</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The orientation of the coordinate system with respect to top, left, and the coordinate number system.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_DiagramLayout"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#DiagramObject.Diagram">
	<rdfs:label xml:lang="en">Diagram</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">A diagram object is part of a diagram.</rdfs:comment>
	<rdfs:domain rdf:resource="#DiagramObject"/>
	<rdfs:range rdf:resource="#Diagram"/>
	<cims:inverseRoleName rdf:resource="#Diagram.DiagramElements"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Diagram.DiagramElements">
	<rdfs:label xml:lang="en">DiagramElements</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">A diagram is made up of multiple diagram objects.</rdfs:comment>
	<rdfs:domain rdf:resource="#Diagram"/>
	<rdfs:range rdf:resource="#DiagramObject"/>
	<cims:inverseRoleName rdf:resource="#DiagramObject.Diagram"/>
	<rdfs:label xml:lang="en">DiagramElements</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#DiagramObject">
	<rdfs:label xml:lang="en">DiagramObject</rdfs:label>
	<rdfs:subClassOf rdf:resource="#IdentifiedObject"/>
	<rdfs:comment  rdf:parseType="Literal">An object that defines one or more points in a given space. This object can be associated with anything that specializes IdentifiedObject. For single line diagrams such objects typically include such items as analog values, breakers, disconnectors, power transformers, and transmission lines.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_DiagramLayout"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#DiagramObject.rotation">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">rotation</rdfs:label>
	<rdfs:domain rdf:resource="#DiagramObject"/>
	<cims:dataType rdf:resource="#AngleDegrees"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Sets the angle of rotation of the diagram object.  Zero degrees is pointing to the top of the diagram.  Rotation is clockwise.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#AngleDegrees">
	<rdfs:label xml:lang="en">AngleDegrees</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Measurement of angle in degrees.</rdfs:comment>
<cims:stereotype>CIMDatatype</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#DiagramObject.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#DiagramObject"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#DiagramObject.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#DiagramObject"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#DiagramObject.DiagramObjectPoints">
	<rdfs:label xml:lang="en">DiagramObjectPoints</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">A diagram object can have 0 or more points to reflect its layout position, routing (for polylines) or boundary (for polygons).</rdfs:comment>
	<rdfs:domain rdf:resource="#DiagramObject"/>
	<rdfs:range rdf:resource="#DiagramObjectPoint"/>
	<cims:inverseRoleName rdf:resource="#DiagramObjectPoint.DiagramObject"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#DiagramObjectPoint.DiagramObject">
	<rdfs:label xml:lang="en">DiagramObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The diagram object with which the points are associated.</rdfs:comment>
	<rdfs:domain rdf:resource="#DiagramObjectPoint"/>
	<rdfs:range rdf:resource="#DiagramObject"/>
	<cims:inverseRoleName rdf:resource="#DiagramObject.DiagramObjectPoints"/>
	<rdfs:label xml:lang="en">DiagramObject</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EnergyConsumer.DiagramObjects">
	<rdfs:label xml:lang="en">DiagramObjects</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The diagram objects that are associated with the domain object.</rdfs:comment>
	<rdfs:domain rdf:resource="#EnergyConsumer"/>
	<rdfs:range rdf:resource="#DiagramObject"/>
	<cims:inverseRoleName rdf:resource="#DiagramObject.IdentifiedObject"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#DiagramObject.IdentifiedObject">
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The domain object to which this diagram object is associated.</rdfs:comment>
	<rdfs:domain rdf:resource="#DiagramObject"/>
	<rdfs:range rdf:resource="#EnergyConsumer"/>
	<cims:inverseRoleName rdf:resource="#EnergyConsumer.DiagramObjects"/>
	<rdfs:label xml:lang="en">IdentifiedObject</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#DiagramObjectPoint">
	<rdfs:label xml:lang="en">DiagramObjectPoint</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">A point in a given space defined by 3 coordinates and associated to a diagram object.  The coordinates may be positive or negative as the origin does not have to be in the corner of a diagram.  </rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_DiagramLayout"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#DiagramObjectPoint.xPosition">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">xPosition</rdfs:label>
	<rdfs:domain rdf:resource="#DiagramObjectPoint"/>
	<cims:dataType rdf:resource="#Float"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The X coordinate of this point.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#DiagramObjectPoint.yPosition">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">yPosition</rdfs:label>
	<rdfs:domain rdf:resource="#DiagramObjectPoint"/>
	<cims:dataType rdf:resource="#Float"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The Y coordinate of this point.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#DiagramObjectPoint.sequenceNumber">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">sequenceNumber</rdfs:label>
	<rdfs:domain rdf:resource="#DiagramObjectPoint"/>
	<cims:dataType rdf:resource="#Integer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The sequence position of the point, used for defining the order of points for diagram objects acting as a polyline or polygon with more than one point.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#OrientationKind">
	<rdfs:label xml:lang="en">OrientationKind</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The orientation of the coordinate system with respect to top, left, and the coordinate number system.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_DiagramLayout"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_Domain">
	<rdfs:label xml:lang="en">Domain</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">The domain package define primitive datatypes that are used by classes in other packages. Stereotypes are used to describe the datatypes. The following stereotypes are defined:
&lt;&lt;enumeration&gt;&gt; A list of permissible constant values.
&lt;&lt;Primitive&gt;&gt; The most basic data types used to compose all other data types.
&lt;&lt;CIMDatatype&gt;&gt; A datatype that contains a value attribute, an optional unit of measure and a unit multiplier. The unit and multiplier may be specified as a static variable initialized to the allowed value.
&lt;&lt;Compound&gt;&gt; A composite of Primitive, enumeration, CIMDatatype or othe Compound classes, as long as the Compound classes do not recurse.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#ActivePowerPerFrequency">
	<rdfs:label xml:lang="en">ActivePowerPerFrequency</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Active power variation with frequency.</rdfs:comment>
<cims:stereotype>CIMDatatype</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ApparentPower">
	<rdfs:label xml:lang="en">ApparentPower</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Product of the RMS value of the voltage and the RMS value of the current.</rdfs:comment>
<cims:stereotype>CIMDatatype</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Conductance">
	<rdfs:label xml:lang="en">Conductance</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Factor by which voltage must be multiplied to give corresponding power lost from a circuit. Real part of admittance.</rdfs:comment>
<cims:stereotype>CIMDatatype</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Length">
	<rdfs:label xml:lang="en">Length</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Unit of length. Never negative.</rdfs:comment>
<cims:stereotype>CIMDatatype</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MonthDay">
	<rdfs:label xml:lang="en">MonthDay</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">MonthDay format as "--mm-dd", which conforms with XSD data type gMonthDay.</rdfs:comment>
<cims:stereotype>Primitive</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PerCent">
	<rdfs:label xml:lang="en">PerCent</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Percentage on a defined base.   For example, specify as 100 to indicate at the defined base.</rdfs:comment>
<cims:stereotype>CIMDatatype</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PerCent.multiplier">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">multiplier</rdfs:label>
	<rdfs:domain rdf:resource="#PerCent"/>
	<cims:dataType rdf:resource="#UnitMultiplier"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<cims:isFixed rdfs:Literal="none" />
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PerCent.unit">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">unit</rdfs:label>
	<rdfs:domain rdf:resource="#PerCent"/>
	<cims:dataType rdf:resource="#UnitSymbol"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<cims:isFixed rdfs:Literal="none" />
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PerCent.value">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">value</rdfs:label>
	<rdfs:domain rdf:resource="#PerCent"/>
	<cims:dataType rdf:resource="#Float"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Normally 0 - 100 on a defined base</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ReactancePerLength">
	<rdfs:label xml:lang="en">ReactancePerLength</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Reactance (imaginary part of impedance) per unit of length, at rated frequency.</rdfs:comment>
<cims:stereotype>CIMDatatype</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Resistance">
	<rdfs:label xml:lang="en">Resistance</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Resistance (real part of impedance).</rdfs:comment>
<cims:stereotype>CIMDatatype</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ResistancePerLength">
	<rdfs:label xml:lang="en">ResistancePerLength</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Resistance (real part of impedance) per unit of length.</rdfs:comment>
<cims:stereotype>CIMDatatype</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Seconds">
	<rdfs:label xml:lang="en">Seconds</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Time, in seconds.</rdfs:comment>
<cims:stereotype>CIMDatatype</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Susceptance">
	<rdfs:label xml:lang="en">Susceptance</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Imaginary part of admittance.</rdfs:comment>
<cims:stereotype>CIMDatatype</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#SusceptancePerLength">
	<rdfs:label xml:lang="en">SusceptancePerLength</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Imaginary part of admittance per unit of length.</rdfs:comment>
<cims:stereotype>CIMDatatype</cims:stereotype>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier">
	<rdfs:label xml:lang="en">UnitMultiplier</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The unit multipliers defined for the CIM.  When applied to unit symbols that already contain a multiplier, both multipliers are used. For example, to exchange kilograms using unit symbol of kg, one uses the "none" multiplier, to exchange metric ton (Mg), one uses the "k" multiplier.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.y">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">y</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-24" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">yocto 10**-24.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.z">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">z</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-21" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">zepto 10**-21.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.a">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">a</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-18" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">atto 10**-18.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.f">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">f</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-15" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">femto 10**-15.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.p">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">p</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-12" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Pico 10**-12.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.P">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">P</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="15" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Peta 10**15</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.E">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">E</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="18" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Exa 10**18.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.Y">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Y</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="24" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Yotta 10**24</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.Z">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Z</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="21" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Zetta 10**21</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.n">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">n</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-9" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Nano 10**-9.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.micro">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">micro</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-6" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Micro 10**-6.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.m">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-3" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Milli 10**-3.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.c">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">c</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-2" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Centi 10**-2.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.d">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">d</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="-1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Deci 10**-1.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.none">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">none</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="0" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">No multiplier or equivalently multiply by 1.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.da">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">da</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">deca 10**1.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.h">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">h</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="2" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">hecto 10**2.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.k">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">k</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="3" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Kilo 10**3.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.M">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">M</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="6" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Mega 10**6.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.G">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">G</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="9" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Giga 10**9.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitMultiplier.T">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">T</rdfs:label>
	<rdfs:domain rdf:resource="#UnitMultiplier"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="12" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Tera 10**12.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol">
	<rdfs:label xml:lang="en">UnitSymbol</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The units defined for usage in the CIM.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Domain"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.m">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="2" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Length in meter.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.kg">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">kg</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="3" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Mass in kilogram.  Note: multiplier “k” is included in this unit symbol for compatibility with IEC 61850-7-3.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.s">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">s</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="4" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Time in seconds.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.A">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">A</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="5" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Current in Ampere.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.K">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">K</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="6" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Temperature in Kelvin.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.mol">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mol</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="7" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Amount of substance in mole.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.cd">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">cd</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="8" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Luminous intensity in candela.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.H">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">H</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="28" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Electric inductance in Henry (Wb/A).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.V">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">V</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="29" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Electric potential in Volt (W/A).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.ohm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ohm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="30" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Electric resistance in ohm (V/A).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.J">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">J</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="31" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Energy in joule (N·m = C·V = W·s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.N">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">N</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="32" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Force in Newton (kg·m/s²).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Hz">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Hz</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="33" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Frequency in Hertz (1/s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.lx">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">lx</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="34" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Illuminance in lux (lm/m²).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.lm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">lm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="35" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Luminous flux in lumen (cd·sr).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Wb">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Wb</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="36" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Magnetic flux in Weber (V·s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.T">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">T</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="37" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Magnetic flux density in Tesla (Wb/m2).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.W">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">W</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="38" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Real power in Watt (J/s). Electrical power may have real and reactive components. The real portion of electrical power (I²R or VIcos(phi)), is expressed in Watts. (See also apparent power and reactive power.)</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.rotPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">rotPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="53" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Rotations per second (1/s). See also Hz (1/s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.radPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">radPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="54" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Angular velocity in radians per second (rad/s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.VA">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">VA</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="61" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Apparent power in Volt Ampere (See also real power and reactive power.)</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.VAr">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">VAr</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="63" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Reactive power in Volt Ampere reactive. The “reactive” or “imaginary” component of electrical power (VIsin(phi)). (See also real power and apparent power).
Note: Different meter designs use different methods to arrive at their results. Some meters may compute reactive power as an arithmetic value, while others compute the value vectorially. The data consumer should determine the method in use and the suitability of the measurement for the intended purpose.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.cosPhi">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">cosPhi</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="65" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Power factor, dimensionless.
Note 1: This definition of power factor only holds for balanced systems. See the alternative definition under code 153.
Note 2 : Beware of differing sign conventions in use between the IEC and EEI. It is assumed that the data consumer understands the type of meter in use and the sign convention in use by the utility.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Vs">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Vs</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="66" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volt second (Ws/A).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.V2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">V2</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="67" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volt squared (W²/A²).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.As">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">As</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="68" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Ampere seconds (A·s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.A2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">A2</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="69" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Ampere squared (A²).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.A2s">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">A2s</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="70" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Ampere squared time in square ampere (A²s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.VAh">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">VAh</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="71" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Apparent energy in Volt Ampere hours.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.dBm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">dBm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="83" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Power level (logrithmic ratio of signal strength , Bel-mW), normalized to 1mW. Note:  multiplier “d” is included in this unit symbol for compatibility with IEC 61850-7-3.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.h">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">h</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="84" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Time, hour = 60 min = 3600 s.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.min">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">min</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="85" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Time, minute  = 60 s.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Q">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Q</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="100" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Quantity power, Q.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Qh">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Qh</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="101" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Quantity energy, Qh.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.ohmm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ohmm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="102" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">resistivity, Ohm metre, (rho).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.APerm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">APerm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="103" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">A/m, magnetic field strength, Ampere per metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.V2h">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">V2h</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="104" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">volt-squared hour, Volt-squared-hours.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.A2h">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">A2h</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="105" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">ampere-squared hour, Ampere-squared hour.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Ah">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Ah</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="106" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Ampere-hours, Ampere-hours.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.count">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">count</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="111" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Amount of substance, Counter value.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.deg">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">deg</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="9" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Plane angle in degrees.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.rad">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">rad</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="10" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Plane angle in radian (m/m).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.sr">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">sr</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="11" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Solid angle in steradian (m2/m2).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Gy">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Gy</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="21" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Absorbed dose in Gray (J/kg).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Bq">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Bq</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="22" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Radioactivity in Becquerel (1/s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.degC">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">degC</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="23" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Relative temperature in degrees Celsius.
In the SI unit system the symbol is ºC. Electric charge is measured in coulomb that has the unit symbol C. To distinguish degree Celsius form coulomb the symbol used in the UML is degC. Reason for not using ºC is the special character º is difficult to manage in software.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Sv">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Sv</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="24" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Dose equivalent in Sievert (J/kg).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.F">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">F</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="25" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Electric capacitance in Farad (C/V).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.C">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">C</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="26" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Electric charge in Coulomb (A·s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.S">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">S</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="27" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Conductance in Siemens.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Pa">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Pa</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="39" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Pressure in Pascal (N/m²). Note: the absolute or relative measurement of pressure is implied with this entry. See below for more explicit forms.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.m2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m2</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="41" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Area in square metre (m²).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.m3">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m3</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="42" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volume in cubic metre (m³).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.mPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="43" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Velocity in metre per second (m/s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.mPers2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mPers2</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="44" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Acceleration in metre per second squared (m/s²).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.m3Pers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m3Pers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="45" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volumetric flow rate in cubic metres per second (m³/s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.mPerm3">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mPerm3</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="46" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Fuel efficiency in metre per cubic metre (m/m³).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.kgm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">kgm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="47" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Moment of mass in kilogram metre (kg·m) (first moment of mass). Note: multiplier “k” is included in this unit symbol for compatibility with IEC 61850-7-3.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.kgPerm3">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">kgPerm3</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="48" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Density in kilogram/cubic metre (kg/m³). Note: multiplier “k” is included in this unit symbol for compatibility with IEC 61850-7-3.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.WPermK">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">WPermK</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="50" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Thermal conductivity in Watt/metre Kelvin.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.JPerK">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">JPerK</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="51" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Heat capacity in Joule/Kelvin.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.ppm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ppm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="52" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Concentration in parts per million.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Wh">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Wh</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="72" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Real energy in Watt hours.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.VArh">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">VArh</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="73" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Reactive energy in Volt Ampere reactive hours.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.VPerHz">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">VPerHz</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="74" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Magnetic flux in Volt per Hertz.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.HzPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">HzPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="75" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Rate of change of frequency in Hertz per second.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.character">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">character</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="76" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Number of characters.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.charPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">charPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="77" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Data rate (baud) in characters per second.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.kgm2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">kgm2</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="78" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Moment of mass in kilogram square metre (kg·m²) (Second moment of mass, commonly called the moment of inertia). Note: multiplier “k” is included in this unit symbol for compatibility with IEC 61850-7-3.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.dB">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">dB</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="79" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Sound pressure level in decibel. Note:  multiplier “d” is included in this unit symbol for compatibility with IEC 61850-7-3.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.WPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">WPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="81" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Ramp rate in Watt per second.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.lPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">lPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="82" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volumetric flow rate in litre per second.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.ft3">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ft3</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="119" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volume, cubic foot.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.m3Perh">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m3Perh</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="125" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volumetric flow rate, cubic metre per hour.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.gal">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">gal</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="128" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volume, US gallon (1 gal = 231 in3 = 128 fl ounce).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Btu">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Btu</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="132" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Energy, British Thermal Unit.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.l">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">l</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="134" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volume, litre = dm3 = m3/1000.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.lPerh">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">lPerh</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="137" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volumetric flow rate, litre per hour.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.lPerl">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">lPerl</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="143" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Concentration, The ratio of the volume of a solute divided by the volume of  the solution. Note: Users may need use a prefix such a ‘µ’ to express a quantity such as ‘µL/L’.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.gPerg">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">gPerg</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="144" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Concentration, The ratio of the mass of a solute divided by the mass of  the solution. Note: Users may need use a prefix such a ‘µ’ to express a quantity such as ‘µg/g’.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.molPerm3">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">molPerm3</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="145" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Concentration, The amount of substance concentration, (c), the amount of solvent in moles divided by the volume of solution in m³.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.molPermol">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">molPermol</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="146" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Concentration, Molar fraction (?), the ratio of the molar amount of a solute divided by the molar amount of the solution.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.molPerkg">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">molPerkg</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="147" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Concentration, Molality, the amount of solute in moles and the amount of solvent in kilograms.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.sPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">sPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="149" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Time, Ratio of time Note: Users may need to supply a prefix such as ‘µ’ to show rates such as ‘µs/s’</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.HzPerHz">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">HzPerHz</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="150" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Frequency, Rate of frequency change  Note: Users may need to supply a prefix such as ‘m’ to show rates such as ‘mHz/Hz’.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.VPerV">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">VPerV</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="151" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Voltage, Ratio of voltages Note: Users may need to supply a prefix such as ‘m’ to show rates such as ‘mV/V’.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.APerA">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">APerA</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="152" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Current, Ratio of Amperages  Note: Users may need to supply a prefix such as ‘m’ to show rates such as ‘mA/A’.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.VPerVA">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">VPerVA</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="153" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Power factor, PF, the ratio of the active power to the apparent power. Note: The sign convention used for power factor will differ between IEC meters and EEI (ANSI) meters. It is assumed that the data consumers understand the type of meter being used and agree on the sign convention in use at any given utility.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.rev">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">rev</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="154" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Amount of rotation, Revolutions.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.kat">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">kat</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="158" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Catalytic activity, katal = mol / s.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.JPerkg">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">JPerkg</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="165" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Specific energy, Joule / kg.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.m3Uncompensated">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m3Uncompensated</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="166" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volume, cubic metre, with the value uncompensated for weather effects.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.m3Compensated">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m3Compensated</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="167" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volume, cubic metre, with the value compensated for weather effects.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.WPerW">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">WPerW</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="168" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Signal Strength, Ratio of power  Note: Users may need to supply a prefix such as ‘m’ to show rates such as ‘mW/W’.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.therm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">therm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="169" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Energy, Therm.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.onePerm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">onePerm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="173" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Wavenumber, reciprocal metre,  (1/m).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.m3Perkg">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m3Perkg</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="174" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Specific volume, cubic metre per kilogram, v.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Pas">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Pas</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="175" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Dynamic viscosity, Pascal second.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Nm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Nm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="176" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Moment of force, Newton metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.NPerm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">NPerm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="177" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Surface tension, Newton per metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.radPers2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">radPers2</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="178" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Angular acceleration, radian per second squared.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.WPerm2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">WPerm2</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="55" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Heat flux density, irradiance, Watt per square metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.JPerkgK">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">JPerkgK</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="60" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Specific heat capacity, specific entropy, Joule per kilogram Kelvin.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.JPerm3">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">JPerm3</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="181" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">energy density, Joule per cubic metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.VPerm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">VPerm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="182" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">electric field strength, Volt per metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.CPerm3">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">CPerm3</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="183" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">electric charge density, Coulomb per cubic metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.CPerm2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">CPerm2</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="184" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">surface charge density, Coulomb per square metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.FPerm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">FPerm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="185" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">permittivity, Farad per metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.HPerm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">HPerm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="186" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">permeability, Henry per metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.JPermol">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">JPermol</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="187" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">molar energy, Joule per mole.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.JPermolK">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">JPermolK</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="188" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">molar entropy, molar heat capacity, Joule per mole kelvin.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.CPerkg">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">CPerkg</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="189" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">exposure (x rays), Coulomb per kilogram.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.GyPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">GyPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="190" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">absorbed dose rate, Gray per second.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.WPersr">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">WPersr</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="191" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Radiant intensity, Watt per steradian.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.WPerm2sr">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">WPerm2sr</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="192" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">radiance, Watt per square metre steradian.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.katPerm3">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">katPerm3</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="193" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">catalytic activity concentration, katal per cubic metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.G">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">G</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="277" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Magnetic flux density, Gauss (1 G = 10-4 T).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Oe">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Oe</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="278" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Magnetic field, Œrsted (1 Oe = (103/4p) A/m).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.WPerA">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">WPerA</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Active power per current flow, watt per Ampere.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.SPerm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">SPerm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="57" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Conductance per length (F/m).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.onePerHz">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">onePerHz</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Reciprocal of frequency (1/Hz).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.VPerVAr">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">VPerVAr</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Power factor, PF, the ratio of the active power to the apparent power. Note: The sign convention used for power factor will differ between IEC meters and EEI (ANSI) meters. It is assumed that the data consumers understand the type of meter being used and agree on the sign convention in use at any given utility.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.ohmPerm">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ohmPerm</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Electric resistance per length in ohm per metre ((V/A)/m).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.kgPerJ">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">kgPerJ</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Weigh per energy in kilogram/joule (kg/J). Note: multiplier “k” is included in this unit symbol for compatibility with IEC 61850-7-3.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.JPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">JPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Energy rate joule per second (J/s),</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.m2Pers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">m2Pers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="49" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Viscosity in metre square / second (m²/s).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.JPerm2">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">JPerm2</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="56" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Insulation energy density, Joule per square metre or watt second per square metre.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.KPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">KPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="58" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Temperature change rate in Kelvin per second.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.d">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">d</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="195" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Time, day = 24 h = 86400 s.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.anglemin">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">anglemin</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="196" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Plane angle, minute.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.anglesec">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">anglesec</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="197" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Plane angle, second.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.ha">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ha</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="198" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Area, hectare.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.tonne">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">tonne</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="199" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">mass, “tonne” or “metric  ton” (1000 kg = 1 Mg).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.bar">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">bar</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="214" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Pressure, bar (1 bar = 100 kPa).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.mmHg">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mmHg</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="215" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Pressure, millimeter of mercury (1 mmHg is approximately 133.3 Pa).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.M">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">M</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="217" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Length, nautical mile (1 M = 1852 m).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.kn">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">kn</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="219" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Speed, knot (1 kn = 1852/3600) m/s.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Vh">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Vh</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="280" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Volt-hour, Volt hours.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.Mx">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Mx</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="276" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Magnetic flux, Maxwell (1 Mx = 10-8 Wb).</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#UnitSymbol.PaPers">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">PaPers</rdfs:label>
	<rdfs:domain rdf:resource="#UnitSymbol"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:isDefault rdfs:Literal="59" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Pressure change rate in Pascal per second.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_Faults">
	<rdfs:label xml:lang="en">Faults</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">The package describe faults that may happen to conducting equipment, e.g. tree falling on a power line.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#EquipmentFault">
	<rdfs:label xml:lang="en">EquipmentFault</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">A fault applied at the terminal, external to the equipment. This class is not used to specify faults internal to the equipment.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Faults"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_LoadModel">
	<rdfs:label xml:lang="en">LoadModel</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">This package is responsible for modeling the energy consumers and the system load as curves and associated curve data. Special circumstances that may affect the load, such as seasons and daytypes, are also included here.

This information is used by Load Forecasting and Load Management.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#ConformLoadGroup">
	<rdfs:label xml:lang="en">ConformLoadGroup</rdfs:label>
	<rdfs:subClassOf rdf:resource="#LoadGroup"/>
	<rdfs:comment  rdf:parseType="Literal">A group of loads conforming to an allocation pattern. </rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_LoadModel"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConformLoadGroup.ConformLoadSchedules">
	<rdfs:label xml:lang="en">ConformLoadSchedules</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The ConformLoadSchedules in the ConformLoadGroup.</rdfs:comment>
	<rdfs:domain rdf:resource="#ConformLoadGroup"/>
	<rdfs:range rdf:resource="#ConformLoadSchedule"/>
	<cims:inverseRoleName rdf:resource="#ConformLoadSchedule.ConformLoadGroup"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..n"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#ofAggregate"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConformLoadSchedule.ConformLoadGroup">
	<rdfs:label xml:lang="en">ConformLoadGroup</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The ConformLoadGroup where the ConformLoadSchedule belongs.</rdfs:comment>
	<rdfs:domain rdf:resource="#ConformLoadSchedule"/>
	<rdfs:range rdf:resource="#ConformLoadGroup"/>
	<cims:inverseRoleName rdf:resource="#ConformLoadGroup.ConformLoadSchedules"/>
	<rdfs:label xml:lang="en">ConformLoadGroup</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConformLoadSchedule">
	<rdfs:label xml:lang="en">ConformLoadSchedule</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">A curve of load  versus time (X-axis) showing the active power values (Y1-axis) and reactive power (Y2-axis) for each unit of the period covered. This curve represents a typical pattern of load over the time period for a given day type and season.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_LoadModel"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConformLoadSchedule.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#ConformLoadSchedule"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConformLoadSchedule.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#ConformLoadSchedule"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConformLoadSchedule.startTime">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">startTime</rdfs:label>
	<rdfs:domain rdf:resource="#ConformLoadSchedule"/>
	<cims:dataType rdf:resource="#DateTime"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The time for the first time point.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConformLoadSchedule.value1Multiplier">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">value1Multiplier</rdfs:label>
	<rdfs:domain rdf:resource="#ConformLoadSchedule"/>
	<cims:dataType rdf:resource="#UnitMultiplier"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Multiplier for value1.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConformLoadSchedule.value1Unit">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">value1Unit</rdfs:label>
	<rdfs:domain rdf:resource="#ConformLoadSchedule"/>
	<cims:dataType rdf:resource="#UnitSymbol"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Value1 units of measure.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConformLoadSchedule.value2Multiplier">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">value2Multiplier</rdfs:label>
	<rdfs:domain rdf:resource="#ConformLoadSchedule"/>
	<cims:dataType rdf:resource="#UnitMultiplier"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Multiplier for value2.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConformLoadSchedule.endTime">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">endTime</rdfs:label>
	<rdfs:domain rdf:resource="#ConformLoadSchedule"/>
	<cims:dataType rdf:resource="#DateTime"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The time for the last time point.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConformLoadSchedule.timeStep">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">timeStep</rdfs:label>
	<rdfs:domain rdf:resource="#ConformLoadSchedule"/>
	<cims:dataType rdf:resource="#Seconds"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The time between each pair of subsequent regular time points in sequence order.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConformLoadSchedule.value2Unit">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">value2Unit</rdfs:label>
	<rdfs:domain rdf:resource="#ConformLoadSchedule"/>
	<cims:dataType rdf:resource="#UnitSymbol"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Value2 units of measure.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#DayType.SeasonDayTypeSchedules">
	<rdfs:label xml:lang="en">SeasonDayTypeSchedules</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Schedules that use this DayType.</rdfs:comment>
	<rdfs:domain rdf:resource="#DayType"/>
	<rdfs:range rdf:resource="#ConformLoadSchedule"/>
	<cims:inverseRoleName rdf:resource="#ConformLoadSchedule.DayType"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConformLoadSchedule.DayType">
	<rdfs:label xml:lang="en">DayType</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">DayType for the Schedule.</rdfs:comment>
	<rdfs:domain rdf:resource="#ConformLoadSchedule"/>
	<rdfs:range rdf:resource="#DayType"/>
	<cims:inverseRoleName rdf:resource="#DayType.SeasonDayTypeSchedules"/>
	<rdfs:label xml:lang="en">DayType</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Season.SeasonDayTypeSchedules">
	<rdfs:label xml:lang="en">SeasonDayTypeSchedules</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Schedules that use this Season.</rdfs:comment>
	<rdfs:domain rdf:resource="#Season"/>
	<rdfs:range rdf:resource="#ConformLoadSchedule"/>
	<cims:inverseRoleName rdf:resource="#ConformLoadSchedule.Season"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ConformLoadSchedule.Season">
	<rdfs:label xml:lang="en">Season</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Season for the Schedule.</rdfs:comment>
	<rdfs:domain rdf:resource="#ConformLoadSchedule"/>
	<rdfs:range rdf:resource="#Season"/>
	<cims:inverseRoleName rdf:resource="#Season.SeasonDayTypeSchedules"/>
	<rdfs:label xml:lang="en">Season</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#DayType">
	<rdfs:label xml:lang="en">DayType</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Group of similar days.   For example it could be used to represent weekdays, weekend, or holidays.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_LoadModel"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#DayType.SeasonDayTypeSchedules">
	<rdfs:label xml:lang="en">SeasonDayTypeSchedules</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Schedules that use this DayType.</rdfs:comment>
	<rdfs:domain rdf:resource="#DayType"/>
	<rdfs:range rdf:resource="#SeasonDayTypeSchedule"/>
	<cims:inverseRoleName rdf:resource="#SeasonDayTypeSchedule.DayType"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#SeasonDayTypeSchedule.DayType">
	<rdfs:label xml:lang="en">DayType</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">DayType for the Schedule.</rdfs:comment>
	<rdfs:domain rdf:resource="#SeasonDayTypeSchedule"/>
	<rdfs:range rdf:resource="#DayType"/>
	<cims:inverseRoleName rdf:resource="#DayType.SeasonDayTypeSchedules"/>
	<rdfs:label xml:lang="en">DayType</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#LoadGroup">
	<rdfs:label xml:lang="en">LoadGroup</rdfs:label>
	<rdfs:subClassOf rdf:resource="#IdentifiedObject"/>
	<rdfs:comment  rdf:parseType="Literal">The class is the third level in a hierarchical structure for grouping of loads for the purpose of load flow load scaling.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_LoadModel"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Season">
	<rdfs:label xml:lang="en">Season</rdfs:label>
	<rdfs:subClassOf rdf:resource="#IdentifiedObject"/>
	<rdfs:comment  rdf:parseType="Literal">A specified time period of the year.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_LoadModel"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Season.endDate">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">endDate</rdfs:label>
	<rdfs:domain rdf:resource="#Season"/>
	<cims:dataType rdf:resource="#MonthDay"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Date season ends.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Season.startDate">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">startDate</rdfs:label>
	<rdfs:domain rdf:resource="#Season"/>
	<cims:dataType rdf:resource="#MonthDay"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Date season starts.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Season.SeasonDayTypeSchedules">
	<rdfs:label xml:lang="en">SeasonDayTypeSchedules</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Schedules that use this Season.</rdfs:comment>
	<rdfs:domain rdf:resource="#Season"/>
	<rdfs:range rdf:resource="#SeasonDayTypeSchedule"/>
	<cims:inverseRoleName rdf:resource="#SeasonDayTypeSchedule.Season"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#SeasonDayTypeSchedule.Season">
	<rdfs:label xml:lang="en">Season</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Season for the Schedule.</rdfs:comment>
	<rdfs:domain rdf:resource="#SeasonDayTypeSchedule"/>
	<rdfs:range rdf:resource="#Season"/>
	<cims:inverseRoleName rdf:resource="#Season.SeasonDayTypeSchedules"/>
	<rdfs:label xml:lang="en">Season</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ScheduledLimitValue.Season">
	<rdfs:label xml:lang="en">Season</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The season for which the scheduled limits applies.    If not specified, then applicable ot any season.</rdfs:comment>
	<rdfs:domain rdf:resource="#ScheduledLimitValue"/>
	<rdfs:range rdf:resource="#Season"/>
	<cims:inverseRoleName rdf:resource="#Season.ScheduledLimits"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
<cims:stereotype>informative</cims:stereotype>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Season.ScheduledLimits">
	<rdfs:label xml:lang="en">ScheduledLimits</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The scheduled limits associated with the season.</rdfs:comment>
	<rdfs:domain rdf:resource="#Season"/>
	<rdfs:range rdf:resource="#ScheduledLimitValue"/>
	<cims:inverseRoleName rdf:resource="#ScheduledLimitValue.Season"/>
	<rdfs:label xml:lang="en">ScheduledLimits</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
<cims:stereotype>informative</cims:stereotype>
	 </rdf:Description>
	 <rdf:Description rdf:about="#SeasonDayTypeSchedule">
	<rdfs:label xml:lang="en">SeasonDayTypeSchedule</rdfs:label>
	<rdfs:subClassOf rdf:resource="#BasicIntervalSchedule"/>
	<rdfs:comment  rdf:parseType="Literal">A time schedule covering a 24 hour period, with curve data for a specific type of season and day.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_LoadModel"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_Meas">
	<rdfs:label xml:lang="en">Meas</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">Contains entities that describe dynamic measurement data exchanged between applications.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#Measurement">
	<rdfs:label xml:lang="en">Measurement</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">A Measurement represents any measured, calculated or non-measured non-calculated quantity. Any piece of equipment may contain Measurements, e.g. a substation may have temperature measurements and door open indications, a transformer may have oil temperature and tank pressure measurements, a bay may contain a number of power flow measurements and a Breaker may contain a switch status measurement. 
The PSR - Measurement association is intended to capture this use of Measurement and is included in the naming hierarchy based on EquipmentContainer. The naming hierarchy typically has Measurements as leafs, e.g. Substation-VoltageLevel-Bay-Switch-Measurement.
Some Measurements represent quantities related to a particular sensor location in the network, e.g. a voltage transformer (PT) at a busbar or a current transformer (CT) at the bar between a breaker and an isolator. The sensing position is not captured in the PSR - Measurement association. Instead it is captured by the Measurement - Terminal association that is used to define the sensing location in the network topology. The location is defined by the connection of the Terminal to ConductingEquipment. 
If both a Terminal and PSR are associated, and the PSR is of type ConductingEquipment, the associated Terminal should belong to that ConductingEquipment instance.
When the sensor location is needed both Measurement-PSR and Measurement-Terminal are used. The Measurement-Terminal association is never used alone.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Meas"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Measurement.unitMultiplier">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">unitMultiplier</rdfs:label>
	<rdfs:domain rdf:resource="#Measurement"/>
	<cims:dataType rdf:resource="#UnitMultiplier"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The unit multiplier of the measured quantity.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Measurement.measurementType">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">measurementType</rdfs:label>
	<rdfs:domain rdf:resource="#Measurement"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Specifies the type of measurement.  For example, this specifies if the measurement represents an indoor temperature, outdoor temperature, bus voltage, line flow, etc.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Measurement.phases">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">phases</rdfs:label>
	<rdfs:domain rdf:resource="#Measurement"/>
	<cims:dataType rdf:resource="#PhaseCode"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Indicates to which phases the measurement applies and avoids the need to use 'measurementType' to also encode phase information (which would explode the types). The phase information in Measurement, along with 'measurementType' and 'phases' uniquely defines a Measurement for a device, based on normal network phase. Their meaning will not change when the computed energizing phasing is changed due to jumpers or other reasons.
If the attribute is missing three phases (ABC) shall be assumed.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Measurement.unitSymbol">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">unitSymbol</rdfs:label>
	<rdfs:domain rdf:resource="#Measurement"/>
	<cims:dataType rdf:resource="#UnitSymbol"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The unit of measure of the measured quantity.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementValue">
	<rdfs:label xml:lang="en">MeasurementValue</rdfs:label>
	<rdfs:subClassOf rdf:resource="#IdentifiedObject"/>
	<rdfs:comment  rdf:parseType="Literal">The current state for a measurement. A state value is an instance of a measurement from a specific source. Measurements can be associated with many state values, each representing a different source for the measurement.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Meas"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementValue.sensorAccuracy">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">sensorAccuracy</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementValue"/>
	<cims:dataType rdf:resource="#PerCent"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The limit, expressed as a percentage of the sensor maximum, that errors will not exceed when the sensor is used under  reference conditions.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#MeasurementValue.timeStamp">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">timeStamp</rdfs:label>
	<rdfs:domain rdf:resource="#MeasurementValue"/>
	<cims:dataType rdf:resource="#DateTime"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The time when the value was last updated</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_OperationalLimits">
	<rdfs:label xml:lang="en">OperationalLimits</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">This package models a specification of limits associated with equipment and other operational entities.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#BranchGroupTerminal">
	<rdfs:label xml:lang="en">BranchGroupTerminal</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">A specific directed terminal flow for a branch group.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_OperationalLimits"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_StateVariables">
	<rdfs:label xml:lang="en">StateVariables</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">State variables for analysis solutions such as powerflow.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#SvPowerFlow">
	<rdfs:label xml:lang="en">SvPowerFlow</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">State variable for power flow. Load convention is used for flow direction. This means flow out from the TopologicalNode into the equipment is positive.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_StateVariables"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_Topology">
	<rdfs:label xml:lang="en">Topology</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">An extension to the Core Package that in association with the Terminal class models Connectivity, that is the physical definition of how equipment is connected together. In addition it models Topology, that is the logical definition of how equipment is connected via closed switches. The Topology definition is independent of the other electrical characteristics.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#TopologicalNode">
	<rdfs:label xml:lang="en">TopologicalNode</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">For a detailed substation model a topological node is a set of connectivity nodes that, in the current network state, are connected together through any type of closed switches, including  jumpers. Topological nodes change as the current network state changes (i.e., switches, breakers, etc. change state).
For a planning model, switch statuses are not used to form topological nodes. Instead they are manually created or deleted in a model builder tool. Topological nodes maintained this way are also called "busses".</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Topology"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_Wires">
	<rdfs:label xml:lang="en">Wires</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">An extension to the Core and Topology package that models information on the electrical characteristics of Transmission and Distribution networks. This package is used by network applications such as State Estimation, Load Flow and Optimal Power Flow.  </rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#ACLineSegment">
	<rdfs:label xml:lang="en">ACLineSegment</rdfs:label>
	<rdfs:subClassOf rdf:resource="#Conductor"/>
	<rdfs:comment  rdf:parseType="Literal">A wire or combination of wires, with consistent electrical characteristics, building a single electrical system, used to carry alternating current between points in the power system.
For symmetrical, transposed 3ph lines, it is sufficient to use  attributes of the line segment, which describe impedances and admittances for the entire length of the segment.  Additionally impedances can be computed by using length and associated per length impedances.
The BaseVoltage at the two ends of ACLineSegments in a Line shall have the same BaseVoltage.nominalVoltage. However, boundary lines  may have slightly different BaseVoltage.nominalVoltages and  variation is allowed. Larger voltage difference in general requires use of an equivalent branch.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ACLineSegment.length">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">length</rdfs:label>
	<rdfs:domain rdf:resource="#ACLineSegment"/>
	<cims:dataType rdf:resource="#Length"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Segment length for calculating line section capabilities</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PerLengthImpedance.ACLineSegments">
	<rdfs:label xml:lang="en">ACLineSegments</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">All line segments described by this per-length impedance.</rdfs:comment>
	<rdfs:domain rdf:resource="#PerLengthImpedance"/>
	<rdfs:range rdf:resource="#ACLineSegment"/>
	<cims:inverseRoleName rdf:resource="#ACLineSegment.PerLengthImpedance"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ACLineSegment.PerLengthImpedance">
	<rdfs:label xml:lang="en">PerLengthImpedance</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Per-length impedance of this line segment.</rdfs:comment>
	<rdfs:domain rdf:resource="#ACLineSegment"/>
	<rdfs:range rdf:resource="#PerLengthImpedance"/>
	<cims:inverseRoleName rdf:resource="#PerLengthImpedance.ACLineSegments"/>
	<rdfs:label xml:lang="en">PerLengthImpedance</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Breaker">
	<rdfs:label xml:lang="en">Breaker</rdfs:label>
	<rdfs:subClassOf rdf:resource="#Switch"/>
	<rdfs:comment  rdf:parseType="Literal">A mechanical switching device capable of making, carrying, and breaking currents under normal circuit conditions and also making, carrying for a specified time, and breaking currents under specified abnormal circuit conditions e.g.  those of short circuit.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Breaker.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#Breaker"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Breaker.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#Breaker"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Breaker.normalOpen">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">normalOpen</rdfs:label>
	<rdfs:domain rdf:resource="#Breaker"/>
	<cims:dataType rdf:resource="#Boolean"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The attribute is used in cases when no Measurement for the status value is present. If the Switch has a status measurement the Discrete.normalValue is expected to match with the Switch.normalOpen.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Breaker.retained">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">retained</rdfs:label>
	<rdfs:domain rdf:resource="#Breaker"/>
	<cims:dataType rdf:resource="#Boolean"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Branch is retained in a bus branch model.  The flow through retained switches will normally be calculated in power flow.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BusbarSection">
	<rdfs:label xml:lang="en">BusbarSection</rdfs:label>
	<rdfs:subClassOf rdf:resource="#ConductingEquipment"/>
	<rdfs:comment  rdf:parseType="Literal">A conductor, or group of conductors, with negligible impedance, that serve to connect other conducting equipment within a single substation. 
Voltage measurements are typically obtained from VoltageTransformers that are connected to busbar sections. A bus bar section may have many physical terminals but for analysis is modelled with exactly one logical terminal.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BusbarSection.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#BusbarSection"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#BusbarSection.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#BusbarSection"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Conductor">
	<rdfs:label xml:lang="en">Conductor</rdfs:label>
	<rdfs:subClassOf rdf:resource="#ConductingEquipment"/>
	<rdfs:comment  rdf:parseType="Literal">Combination of conducting material with consistent electrical characteristics, building a single electrical system, used to carry current between points in the power system.  </rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Conductor.length">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">length</rdfs:label>
	<rdfs:domain rdf:resource="#Conductor"/>
	<cims:dataType rdf:resource="#Length"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Segment length for calculating line section capabilities</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Disconnector">
	<rdfs:label xml:lang="en">Disconnector</rdfs:label>
	<rdfs:subClassOf rdf:resource="#Switch"/>
	<rdfs:comment  rdf:parseType="Literal">A manually operated or motor operated mechanical switching device used for changing the connections in a circuit, or for isolating a circuit or equipment from a source of power. It is required to open or close circuits when negligible current is broken or made. </rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Disconnector.open">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">open</rdfs:label>
	<rdfs:domain rdf:resource="#Disconnector"/>
	<cims:dataType rdf:resource="#Boolean"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The attribute tells if the switch is considered open when used as input to topology processing.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Disconnector.retained">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">retained</rdfs:label>
	<rdfs:domain rdf:resource="#Disconnector"/>
	<cims:dataType rdf:resource="#Boolean"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Branch is retained in a bus branch model.  The flow through retained switches will normally be calculated in power flow.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Disconnector.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#Disconnector"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Disconnector.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#Disconnector"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EnergyConsumer">
	<rdfs:label xml:lang="en">EnergyConsumer</rdfs:label>
	<rdfs:subClassOf rdf:resource="#ConductingEquipment"/>
	<rdfs:comment  rdf:parseType="Literal">Generic user of energy - a  point of consumption on the power system model.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EnergyConsumer.p">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">p</rdfs:label>
	<rdfs:domain rdf:resource="#EnergyConsumer"/>
	<cims:dataType rdf:resource="#ActivePower"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Active power of the load. Load sign convention is used, i.e. positive sign means flow out from a node.
For voltage dependent loads the value is at rated voltage.
Starting value for a steady state solution.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EnergySource">
	<rdfs:label xml:lang="en">EnergySource</rdfs:label>
	<rdfs:subClassOf rdf:resource="#ConductingEquipment"/>
	<rdfs:comment  rdf:parseType="Literal">A generic equivalent for an energy supplier on a transmission or distribution voltage level.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EnergySource.activePower">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">activePower</rdfs:label>
	<rdfs:domain rdf:resource="#EnergySource"/>
	<cims:dataType rdf:resource="#ActivePower"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">High voltage source active injection. Load sign convention is used, i.e. positive sign means flow out from a node.
Starting value for steady state solutions.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EnergySource.nominalVoltage">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">nominalVoltage</rdfs:label>
	<rdfs:domain rdf:resource="#EnergySource"/>
	<cims:dataType rdf:resource="#Voltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Phase-to-phase nominal voltage.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EnergySource.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#EnergySource"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#EnergySource.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#EnergySource"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ExternalNetworkInjection">
	<rdfs:label xml:lang="en">ExternalNetworkInjection</rdfs:label>
	<rdfs:subClassOf rdf:resource="#RegulatingCondEq"/>
	<rdfs:comment  rdf:parseType="Literal">This class represents external network and it is used for IEC 60909 calculations.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ExternalNetworkInjection.governorSCD">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">governorSCD</rdfs:label>
	<rdfs:domain rdf:resource="#ExternalNetworkInjection"/>
	<cims:dataType rdf:resource="#ActivePowerPerFrequency"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Power Frequency Bias. This is the change in power injection divided by the change in frequency and negated.  A positive value of the power frequency bias provides additional power injection upon a drop in frequency.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ExternalNetworkInjection.maxP">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">maxP</rdfs:label>
	<rdfs:domain rdf:resource="#ExternalNetworkInjection"/>
	<cims:dataType rdf:resource="#ActivePower"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Maximum active power of the injection.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ExternalNetworkInjection.maxQ">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">maxQ</rdfs:label>
	<rdfs:domain rdf:resource="#ExternalNetworkInjection"/>
	<cims:dataType rdf:resource="#ReactivePower"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Not for short circuit modelling; It is used for modelling of infeed for load flow exchange. If maxQ and minQ are not used ReactiveCapabilityCurve can be used </rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ExternalNetworkInjection.maxR0ToX0Ratio">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">maxR0ToX0Ratio</rdfs:label>
	<rdfs:domain rdf:resource="#ExternalNetworkInjection"/>
	<cims:dataType rdf:resource="#Float"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Maximum ratio of zero sequence resistance of Network Feeder to its zero sequence reactance (R(0)/X(0) max). Used for short circuit data exchange according to IEC 60909</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ExternalNetworkInjection.maxR1ToX1Ratio">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">maxR1ToX1Ratio</rdfs:label>
	<rdfs:domain rdf:resource="#ExternalNetworkInjection"/>
	<cims:dataType rdf:resource="#Float"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Maximum ratio of positive sequence resistance of Network Feeder to its positive sequence reactance (R(1)/X(1) max). Used for short circuit data exchange according to IEC 60909</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ExternalNetworkInjection.minP">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">minP</rdfs:label>
	<rdfs:domain rdf:resource="#ExternalNetworkInjection"/>
	<cims:dataType rdf:resource="#ActivePower"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Minimum active power of the injection.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ExternalNetworkInjection.minQ">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">minQ</rdfs:label>
	<rdfs:domain rdf:resource="#ExternalNetworkInjection"/>
	<cims:dataType rdf:resource="#ReactivePower"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Not for short circuit modelling; It is used for modelling of infeed for load flow exchange. If maxQ and minQ are not used ReactiveCapabilityCurve can be used</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ExternalNetworkInjection.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#ExternalNetworkInjection"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ExternalNetworkInjection.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#ExternalNetworkInjection"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ExternalNetworkInjection.RegulatingControl">
	<rdfs:label xml:lang="en">RegulatingControl</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The regulating control scheme in which this equipment participates.</rdfs:comment>
	<rdfs:domain rdf:resource="#ExternalNetworkInjection"/>
	<rdfs:range rdf:resource="#RegulatingControl"/>
	<cims:inverseRoleName rdf:resource="#RegulatingControl.RegulatingCondEq"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RegulatingControl.RegulatingCondEq">
	<rdfs:label xml:lang="en">RegulatingCondEq</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The equipment that participates in this regulating control scheme.</rdfs:comment>
	<rdfs:domain rdf:resource="#RegulatingControl"/>
	<rdfs:range rdf:resource="#ExternalNetworkInjection"/>
	<cims:inverseRoleName rdf:resource="#ExternalNetworkInjection.RegulatingControl"/>
	<rdfs:label xml:lang="en">RegulatingCondEq</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Fuse">
	<rdfs:label xml:lang="en">Fuse</rdfs:label>
	<rdfs:subClassOf rdf:resource="#Switch"/>
	<rdfs:comment  rdf:parseType="Literal">An overcurrent protective device with a circuit opening fusible part that is heated and severed by the passage of overcurrent through it. A fuse is considered a switching device because it breaks current.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Fuse.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#Fuse"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Fuse.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#Fuse"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Fuse.ratedCurrent">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ratedCurrent</rdfs:label>
	<rdfs:domain rdf:resource="#Fuse"/>
	<cims:dataType rdf:resource="#CurrentFlow"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The maximum continuous current carrying capacity in amps governed by the device material and construction.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Fuse.open">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">open</rdfs:label>
	<rdfs:domain rdf:resource="#Fuse"/>
	<cims:dataType rdf:resource="#Boolean"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The attribute tells if the switch is considered open when used as input to topology processing.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Fuse.retained">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">retained</rdfs:label>
	<rdfs:domain rdf:resource="#Fuse"/>
	<cims:dataType rdf:resource="#Boolean"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Branch is retained in a bus branch model.  The flow through retained switches will normally be calculated in power flow.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#GroundDisconnector">
	<rdfs:label xml:lang="en">GroundDisconnector</rdfs:label>
	<rdfs:subClassOf rdf:resource="#Switch"/>
	<rdfs:comment  rdf:parseType="Literal">A manually operated or motor operated mechanical switching device used for isolating a circuit or equipment from ground.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#GroundDisconnector.open">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">open</rdfs:label>
	<rdfs:domain rdf:resource="#GroundDisconnector"/>
	<cims:dataType rdf:resource="#Boolean"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The attribute tells if the switch is considered open when used as input to topology processing.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#GroundDisconnector.retained">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">retained</rdfs:label>
	<rdfs:domain rdf:resource="#GroundDisconnector"/>
	<cims:dataType rdf:resource="#Boolean"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Branch is retained in a bus branch model.  The flow through retained switches will normally be calculated in power flow.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#GroundDisconnector.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#GroundDisconnector"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#GroundDisconnector.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#GroundDisconnector"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Line">
	<rdfs:label xml:lang="en">Line</rdfs:label>
	<rdfs:subClassOf rdf:resource="#IdentifiedObject"/>
	<rdfs:comment  rdf:parseType="Literal">Contains equipment beyond a substation belonging to a power transmission line. </rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Line.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#Line"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Line.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#Line"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#LinearShuntCompensator">
	<rdfs:label xml:lang="en">LinearShuntCompensator</rdfs:label>
	<rdfs:subClassOf rdf:resource="#ConductingEquipment"/>
	<rdfs:comment  rdf:parseType="Literal">A linear shunt compensator has banks or sections with equal admittance values.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#LinearShuntCompensator.gPerSection">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">gPerSection</rdfs:label>
	<rdfs:domain rdf:resource="#LinearShuntCompensator"/>
	<cims:dataType rdf:resource="#Conductance"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Positive sequence shunt (charging) conductance per section</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#LinearShuntCompensator.bPerSection">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">bPerSection</rdfs:label>
	<rdfs:domain rdf:resource="#LinearShuntCompensator"/>
	<cims:dataType rdf:resource="#Susceptance"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Positive sequence shunt (charging) susceptance per section</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#LinearShuntCompensator.nomU">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">nomU</rdfs:label>
	<rdfs:domain rdf:resource="#LinearShuntCompensator"/>
	<cims:dataType rdf:resource="#Voltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The voltage at which the nominal reactive power may be calculated. This should normally be within 10% of the voltage at which the capacitor is connected to the network.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#LinearShuntCompensator.phaseConnection">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">phaseConnection</rdfs:label>
	<rdfs:domain rdf:resource="#LinearShuntCompensator"/>
	<cims:dataType rdf:resource="#PhaseShuntConnectionKind"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The type of phase connection, such as wye or delta.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseShuntConnectionKind">
	<rdfs:label xml:lang="en">PhaseShuntConnectionKind</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The configuration of phase connections for a single terminal device such as a load or capacitor.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#LinearShuntCompensator.sections">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">sections</rdfs:label>
	<rdfs:domain rdf:resource="#LinearShuntCompensator"/>
	<cims:dataType rdf:resource="#Float"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Shunt compensator sections in use.
Starting value for steady state solution. Non integer values are allowed to support continuous variables. The reasons for continuous value are to support study cases where no discrete shunt compensators has yet been designed, a solutions where a narrow voltage band force the sections to oscillate or accommodate for a continuous solution as input.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#LinearShuntCompensator.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#LinearShuntCompensator"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#LinearShuntCompensator.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#LinearShuntCompensator"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#LinearShuntCompensator.ShuntCompensatorPhase">
	<rdfs:label xml:lang="en">ShuntCompensatorPhase</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The individual phases models for the shunt compensator.</rdfs:comment>
	<rdfs:domain rdf:resource="#LinearShuntCompensator"/>
	<rdfs:range rdf:resource="#ShuntCompensatorPhase"/>
	<cims:inverseRoleName rdf:resource="#ShuntCompensatorPhase.ShuntCompensator"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ShuntCompensatorPhase.ShuntCompensator">
	<rdfs:label xml:lang="en">ShuntCompensator</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Shunt compensator of this shunt compensator phase.</rdfs:comment>
	<rdfs:domain rdf:resource="#ShuntCompensatorPhase"/>
	<rdfs:range rdf:resource="#LinearShuntCompensator"/>
	<cims:inverseRoleName rdf:resource="#LinearShuntCompensator.ShuntCompensatorPhase"/>
	<rdfs:label xml:lang="en">ShuntCompensator</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#LoadBreakSwitch">
	<rdfs:label xml:lang="en">LoadBreakSwitch</rdfs:label>
	<rdfs:subClassOf rdf:resource="#Switch"/>
	<rdfs:comment  rdf:parseType="Literal">A mechanical switching device capable of making, carrying, and breaking currents under normal operating conditions.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#LoadBreakSwitch.retained">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">retained</rdfs:label>
	<rdfs:domain rdf:resource="#LoadBreakSwitch"/>
	<cims:dataType rdf:resource="#Boolean"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Branch is retained in a bus branch model.  The flow through retained switches will normally be calculated in power flow.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#LoadBreakSwitch.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#LoadBreakSwitch"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#LoadBreakSwitch.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#LoadBreakSwitch"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PerLengthImpedance">
	<rdfs:label xml:lang="en">PerLengthImpedance</rdfs:label>
	<rdfs:subClassOf rdf:resource="#IdentifiedObject"/>
	<rdfs:comment  rdf:parseType="Literal">Common type for per-length impedance electrical catalogues.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PerLengthImpedance.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#PerLengthImpedance"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PerLengthImpedance.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#PerLengthImpedance"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PerLengthLineParameter">
	<rdfs:label xml:lang="en">PerLengthLineParameter</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Common type for per-length electrical catalogues describing line parameters.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PerLengthSequenceImpedance">
	<rdfs:label xml:lang="en">PerLengthSequenceImpedance</rdfs:label>
	<rdfs:subClassOf rdf:resource="#PerLengthImpedance"/>
	<rdfs:comment  rdf:parseType="Literal">Sequence impedance and admittance parameters per unit length, for transposed lines of 1, 2, or 3 phases. For 1-phase lines, define x=x0=xself. For 2-phase lines, define x=xs-xm and x0=xs+xm.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PerLengthSequenceImpedance.b0ch">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">b0ch</rdfs:label>
	<rdfs:domain rdf:resource="#PerLengthSequenceImpedance"/>
	<cims:dataType rdf:resource="#SusceptancePerLength"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Zero sequence shunt (charging) susceptance, per unit of length.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PerLengthSequenceImpedance.bch">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">bch</rdfs:label>
	<rdfs:domain rdf:resource="#PerLengthSequenceImpedance"/>
	<cims:dataType rdf:resource="#SusceptancePerLength"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Positive sequence shunt (charging) susceptance, per unit of length.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PerLengthSequenceImpedance.r">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">r</rdfs:label>
	<rdfs:domain rdf:resource="#PerLengthSequenceImpedance"/>
	<cims:dataType rdf:resource="#ResistancePerLength"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Positive sequence series resistance, per unit of length.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PerLengthSequenceImpedance.x">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">x</rdfs:label>
	<rdfs:domain rdf:resource="#PerLengthSequenceImpedance"/>
	<cims:dataType rdf:resource="#ReactancePerLength"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Positive sequence series reactance, per unit of length.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PhaseShuntConnectionKind">
	<rdfs:label xml:lang="en">PhaseShuntConnectionKind</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The configuration of phase connections for a single terminal device such as a load or capacitor.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PowerTransformer">
	<rdfs:label xml:lang="en">PowerTransformer</rdfs:label>
	<rdfs:subClassOf rdf:resource="#Equipment"/>
	<rdfs:comment  rdf:parseType="Literal">An electrical device consisting of  two or more coupled windings, with or without a magnetic core, for introducing mutual coupling between electric circuits. Transformers can be used to control voltage and phase shift (active power flow).
A power transformer may be composed of separate transformer tanks that need not be identical.
A power transformer can be modeled with or without tanks and is intended for use in both balanced and unbalanced representations.   A power transformer typically has two terminals, but may have one (grounding), three or more terminals.
The inherited association ConductingEquipment.BaseVoltage should not be used.  The association from TransformerEnd to BaseVoltage should be used instead.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PowerTransformer.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#PowerTransformer"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PowerTransformer.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#PowerTransformer"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PowerTransformer.PowerTransformerEnd">
	<rdfs:label xml:lang="en">PowerTransformerEnd</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The ends of this power transformer.</rdfs:comment>
	<rdfs:domain rdf:resource="#PowerTransformer"/>
	<rdfs:range rdf:resource="#PowerTransformerEnd"/>
	<cims:inverseRoleName rdf:resource="#PowerTransformerEnd.PowerTransformer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PowerTransformerEnd.PowerTransformer">
	<rdfs:label xml:lang="en">PowerTransformer</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The power transformer of this power transformer end.</rdfs:comment>
	<rdfs:domain rdf:resource="#PowerTransformerEnd"/>
	<rdfs:range rdf:resource="#PowerTransformer"/>
	<cims:inverseRoleName rdf:resource="#PowerTransformer.PowerTransformerEnd"/>
	<rdfs:label xml:lang="en">PowerTransformer</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PowerTransformerEnd">
	<rdfs:label xml:lang="en">PowerTransformerEnd</rdfs:label>
	<rdfs:subClassOf rdf:resource="#TransformerEnd"/>
	<rdfs:comment  rdf:parseType="Literal">A PowerTransformerEnd is associated with each Terminal of a PowerTransformer.
The impedance values r, r0, x, and x0 of a PowerTransformerEnd represents a star equivalent as follows
1) for a two Terminal PowerTransformer the high voltage PowerTransformerEnd has non zero values on r, r0, x, and x0 while the low voltage PowerTransformerEnd has zero values for r, r0, x, and x0.
2) for a three Terminal PowerTransformer the three PowerTransformerEnds represents a star equivalent with each leg in the star represented by r, r0, x, and x0 values.
3) for a PowerTransformer with more than three Terminals the PowerTransformerEnd impedance values cannot be used. Instead use the TransformerMeshImpedance or split the transformer into multiple PowerTransformers.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PowerTransformerEnd.ratedS">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ratedS</rdfs:label>
	<rdfs:domain rdf:resource="#PowerTransformerEnd"/>
	<cims:dataType rdf:resource="#ApparentPower"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Normal apparent power rating.
The attribute shall be a positive value. For a two-winding transformer the values for the high and low voltage sides shall be identical. </rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PowerTransformerEnd.ratedU">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ratedU</rdfs:label>
	<rdfs:domain rdf:resource="#PowerTransformerEnd"/>
	<cims:dataType rdf:resource="#Voltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Rated voltage: phase-phase for three-phase windings, and either phase-phase or phase-neutral for single-phase windings.
A high voltage side, as given by TransformerEnd.endNumber, shall have a ratedU that is greater or equal than ratedU for the lower voltage sides.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PowerTransformerEnd.r">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">r</rdfs:label>
	<rdfs:domain rdf:resource="#PowerTransformerEnd"/>
	<cims:dataType rdf:resource="#Resistance"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Resistance (star-model) of the transformer end.
The attribute shall be equal or greater than zero for non-equivalent transformers.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PowerTransformerEnd.phaseAngleClock">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">phaseAngleClock</rdfs:label>
	<rdfs:domain rdf:resource="#PowerTransformerEnd"/>
	<cims:dataType rdf:resource="#Integer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Terminal voltage phase angle displacement where 360 degrees are represented with clock hours. The valid values are 0 to 11. For example, for the secondary side end of a transformer with vector group code of 'Dyn11', specify the connection kind as wye with neutral and specify the phase angle of the clock as 11.  The clock value of the transformer end number specified as 1, is assumed to be zero.  Note the transformer end number is not assumed to be the same as the terminal sequence number.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PowerTransformerEnd.connectionKind">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">connectionKind</rdfs:label>
	<rdfs:domain rdf:resource="#PowerTransformerEnd"/>
	<cims:dataType rdf:resource="#WindingConnection"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Kind of connection.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WindingConnection">
	<rdfs:label xml:lang="en">WindingConnection</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Winding connection type.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WindingConnection.Y">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Y</rdfs:label>
	<rdfs:domain rdf:resource="#WindingConnection"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Wye</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WindingConnection.Z">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Z</rdfs:label>
	<rdfs:domain rdf:resource="#WindingConnection"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">ZigZag</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WindingConnection.Yn">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Yn</rdfs:label>
	<rdfs:domain rdf:resource="#WindingConnection"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Wye, with neutral brought out for grounding.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WindingConnection.Zn">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Zn</rdfs:label>
	<rdfs:domain rdf:resource="#WindingConnection"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">ZigZag, with neutral brought out for grounding.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WindingConnection.A">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">A</rdfs:label>
	<rdfs:domain rdf:resource="#WindingConnection"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Autotransformer common winding</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WindingConnection.I">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">I</rdfs:label>
	<rdfs:domain rdf:resource="#WindingConnection"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Independent winding, for single-phase connections</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WindingConnection.D">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">D</rdfs:label>
	<rdfs:domain rdf:resource="#WindingConnection"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Delta</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PowerTransformerEnd.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#PowerTransformerEnd"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#PowerTransformerEnd.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#PowerTransformerEnd"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RatioTapChanger">
	<rdfs:label xml:lang="en">RatioTapChanger</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">A tap changer that changes the voltage ratio impacting the voltage magnitude but not the phase angle across the transformer.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RatioTapChanger.tculControlMode">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">tculControlMode</rdfs:label>
	<rdfs:domain rdf:resource="#RatioTapChanger"/>
	<cims:dataType rdf:resource="#TransformerControlMode"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Specifies the regulation control mode (voltage or reactive) of the RatioTapChanger.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#TransformerControlMode">
	<rdfs:label xml:lang="en">TransformerControlMode</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Control modes for a transformer.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#TransformerControlMode.volt">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">volt</rdfs:label>
	<rdfs:domain rdf:resource="#TransformerControlMode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Voltage control</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#TransformerControlMode.reactive">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">reactive</rdfs:label>
	<rdfs:domain rdf:resource="#TransformerControlMode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Reactive power flow control</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RatioTapChanger.stepVoltageIncrement">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">stepVoltageIncrement</rdfs:label>
	<rdfs:domain rdf:resource="#RatioTapChanger"/>
	<cims:dataType rdf:resource="#PerCent"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Tap step increment, in per cent of nominal voltage, per step position.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RatioTapChanger.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#RatioTapChanger"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RatioTapChanger.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#RatioTapChanger"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RatioTapChanger.highStep">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">highStep</rdfs:label>
	<rdfs:domain rdf:resource="#RatioTapChanger"/>
	<cims:dataType rdf:resource="#Integer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Highest possible tap step position, advance from neutral.
The attribute shall be greater than lowStep.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RatioTapChanger.lowStep">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">lowStep</rdfs:label>
	<rdfs:domain rdf:resource="#RatioTapChanger"/>
	<cims:dataType rdf:resource="#Integer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Lowest possible tap step position, retard from neutral</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RatioTapChanger.ltcFlag">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ltcFlag</rdfs:label>
	<rdfs:domain rdf:resource="#RatioTapChanger"/>
	<cims:dataType rdf:resource="#Boolean"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Specifies whether or not a TapChanger has load tap changing capabilities.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RatioTapChanger.neutralStep">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">neutralStep</rdfs:label>
	<rdfs:domain rdf:resource="#RatioTapChanger"/>
	<cims:dataType rdf:resource="#Integer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The neutral tap step position for this winding.
The attribute shall be equal or greater than lowStep and equal or less than highStep.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RatioTapChanger.neutralU">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">neutralU</rdfs:label>
	<rdfs:domain rdf:resource="#RatioTapChanger"/>
	<cims:dataType rdf:resource="#Voltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Voltage at which the winding operates at the neutral tap setting.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RatioTapChanger.normalStep">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">normalStep</rdfs:label>
	<rdfs:domain rdf:resource="#RatioTapChanger"/>
	<cims:dataType rdf:resource="#Integer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The tap step position used in "normal" network operation for this winding. For a "Fixed" tap changer indicates the current physical tap setting.
The attribute shall be equal or greater than lowStep and equal or less than highStep.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RatioTapChanger.step">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">step</rdfs:label>
	<rdfs:domain rdf:resource="#RatioTapChanger"/>
	<cims:dataType rdf:resource="#Float"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Tap changer position.
Starting step for a steady state solution. Non integer values are allowed to support continuous tap variables. The reasons for continuous value are to support study cases where no discrete tap changers has yet been designed, a solutions where a narrow voltage band force the tap step to oscillate or accommodate for a continuous solution as input.
The attribute shall be equal or greater than lowStep and equal or less than highStep.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#TransformerEnd.RatioTapChanger">
	<rdfs:label xml:lang="en">RatioTapChanger</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Ratio tap changer associated with this transformer end.</rdfs:comment>
	<rdfs:domain rdf:resource="#TransformerEnd"/>
	<rdfs:range rdf:resource="#RatioTapChanger"/>
	<cims:inverseRoleName rdf:resource="#RatioTapChanger.TransformerEnd"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RatioTapChanger.TransformerEnd">
	<rdfs:label xml:lang="en">TransformerEnd</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Transformer end to which this ratio tap changer belongs.</rdfs:comment>
	<rdfs:domain rdf:resource="#RatioTapChanger"/>
	<rdfs:range rdf:resource="#TransformerEnd"/>
	<cims:inverseRoleName rdf:resource="#TransformerEnd.RatioTapChanger"/>
	<rdfs:label xml:lang="en">TransformerEnd</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RegulatingCondEq">
	<rdfs:label xml:lang="en">RegulatingCondEq</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">A type of conducting equipment that can regulate a quantity (i.e. voltage or flow) at a specific point in the network. </rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RegulatingCondEq.RegulatingControl">
	<rdfs:label xml:lang="en">RegulatingControl</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The regulating control scheme in which this equipment participates.</rdfs:comment>
	<rdfs:domain rdf:resource="#RegulatingCondEq"/>
	<rdfs:range rdf:resource="#RegulatingControl"/>
	<cims:inverseRoleName rdf:resource="#RegulatingControl.RegulatingCondEq"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RegulatingControl.RegulatingCondEq">
	<rdfs:label xml:lang="en">RegulatingCondEq</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">The equipment that participates in this regulating control scheme.</rdfs:comment>
	<rdfs:domain rdf:resource="#RegulatingControl"/>
	<rdfs:range rdf:resource="#RegulatingCondEq"/>
	<cims:inverseRoleName rdf:resource="#RegulatingCondEq.RegulatingControl"/>
	<rdfs:label xml:lang="en">RegulatingCondEq</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RegulatingControl">
	<rdfs:label xml:lang="en">RegulatingControl</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Specifies a set of equipment that works together to control a power system quantity such as voltage or flow. 
Remote bus voltage control is possible by specifying the controlled terminal located at some place remote from the controlling equipment.
In case multiple equipment, possibly of different types, control same terminal there must be only one RegulatingControl at that terminal. The most specific subtype of RegulatingControl shall be used in case such equipment participate in the control, e.g. TapChangerControl for tap changers.
For flow control  load sign convention is used, i.e. positive sign means flow out from a TopologicalNode (bus) into the conducting equipment.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RegulatingControl.mRID">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">mRID</rdfs:label>
	<rdfs:domain rdf:resource="#RegulatingControl"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Master resource identifier issued by a model authority. The mRID is unique within an exchange context. Global uniqueness is easily achieved by using a UUID,  as specified in RFC 4122, for the mRID. The use of UUID is strongly recommended.
For CIMXML data files in RDF syntax conforming to IEC 61970-552 Edition 1, the mRID is mapped to rdf:ID or rdf:about attributes that identify CIM object elements.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#RegulatingControl.name">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">name</rdfs:label>
	<rdfs:domain rdf:resource="#RegulatingControl"/>
	<cims:dataType rdf:resource="#String"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The name is any free human readable and possibly non unique text naming the object.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ShuntCompensator">
	<rdfs:label xml:lang="en">ShuntCompensator</rdfs:label>
	<rdfs:subClassOf rdf:resource="#RegulatingCondEq"/>
	<rdfs:comment  rdf:parseType="Literal">A shunt capacitor or reactor or switchable bank of shunt capacitors or reactors. A section of a shunt compensator is an individual capacitor or reactor.  A negative value for reactivePerSection indicates that the compensator is a reactor. ShuntCompensator is a single terminal device.  Ground is implied.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ShuntCompensator.nomU">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">nomU</rdfs:label>
	<rdfs:domain rdf:resource="#ShuntCompensator"/>
	<cims:dataType rdf:resource="#Voltage"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The voltage at which the nominal reactive power may be calculated. This should normally be within 10% of the voltage at which the capacitor is connected to the network.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ShuntCompensator.phaseConnection">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">phaseConnection</rdfs:label>
	<rdfs:domain rdf:resource="#ShuntCompensator"/>
	<cims:dataType rdf:resource="#PhaseShuntConnectionKind"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The type of phase connection, such as wye or delta.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ShuntCompensator.sections">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">sections</rdfs:label>
	<rdfs:domain rdf:resource="#ShuntCompensator"/>
	<cims:dataType rdf:resource="#Float"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Shunt compensator sections in use.
Starting value for steady state solution. Non integer values are allowed to support continuous variables. The reasons for continuous value are to support study cases where no discrete shunt compensators has yet been designed, a solutions where a narrow voltage band force the sections to oscillate or accommodate for a continuous solution as input.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#ShuntCompensatorPhase">
	<rdfs:label xml:lang="en">ShuntCompensatorPhase</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Single phase of a multi-phase shunt compensator when its attributes might be different per phase.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Switch">
	<rdfs:label xml:lang="en">Switch</rdfs:label>
	<rdfs:subClassOf rdf:resource="#ConductingEquipment"/>
	<rdfs:comment  rdf:parseType="Literal">A generic device designed to close, or open, or both, one or more electric circuits.  All switches are two terminal devices including grounding switches.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Switch.normalOpen">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">normalOpen</rdfs:label>
	<rdfs:domain rdf:resource="#Switch"/>
	<cims:dataType rdf:resource="#Boolean"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The attribute is used in cases when no Measurement for the status value is present. If the Switch has a status measurement the Discrete.normalValue is expected to match with the Switch.normalOpen.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Switch.retained">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">retained</rdfs:label>
	<rdfs:domain rdf:resource="#Switch"/>
	<cims:dataType rdf:resource="#Boolean"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Branch is retained in a bus branch model.  The flow through retained switches will normally be calculated in power flow.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Switch.ratedCurrent">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">ratedCurrent</rdfs:label>
	<rdfs:domain rdf:resource="#Switch"/>
	<cims:dataType rdf:resource="#CurrentFlow"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">The maximum continuous current carrying capacity in amps governed by the device material and construction.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#TransformerControlMode">
	<rdfs:label xml:lang="en">TransformerControlMode</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Control modes for a transformer.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#TransformerControlMode.volt">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">volt</rdfs:label>
	<rdfs:domain rdf:resource="#TransformerControlMode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Voltage control</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#TransformerControlMode.reactive">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">reactive</rdfs:label>
	<rdfs:domain rdf:resource="#TransformerControlMode"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Reactive power flow control</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#TransformerEnd">
	<rdfs:label xml:lang="en">TransformerEnd</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">A conducting connection point of a power transformer. It corresponds to a physical transformer winding terminal.  In earlier CIM versions, the TransformerWinding class served a similar purpose, but this class is more flexible because it associates to terminal but is not a specialization of ConductingEquipment.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#TransformerEnd.endNumber">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">endNumber</rdfs:label>
	<rdfs:domain rdf:resource="#TransformerEnd"/>
	<cims:dataType rdf:resource="#Integer"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">Number for this transformer end, corresponding to the end's order in the power transformer vector group or phase angle clock number.  Highest voltage winding should be 1.  Each end within a power transformer should have a unique subsequent end number.   Note the transformer end number need not match the terminal sequence number.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#TransformerEnd.grounded">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">grounded</rdfs:label>
	<rdfs:domain rdf:resource="#TransformerEnd"/>
	<cims:dataType rdf:resource="#Boolean"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..1" />
	<rdfs:comment  rdf:parseType="Literal">(for Yn and Zn connections) True if the neutral is solidly grounded.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WindingConnection">
	<rdfs:label xml:lang="en">WindingConnection</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Winding connection type.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_Wires"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WindingConnection.Y">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Y</rdfs:label>
	<rdfs:domain rdf:resource="#WindingConnection"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Wye</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WindingConnection.Z">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Z</rdfs:label>
	<rdfs:domain rdf:resource="#WindingConnection"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">ZigZag</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WindingConnection.Yn">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Yn</rdfs:label>
	<rdfs:domain rdf:resource="#WindingConnection"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Wye, with neutral brought out for grounding.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WindingConnection.Zn">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">Zn</rdfs:label>
	<rdfs:domain rdf:resource="#WindingConnection"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">ZigZag, with neutral brought out for grounding.</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WindingConnection.A">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">A</rdfs:label>
	<rdfs:domain rdf:resource="#WindingConnection"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Autotransformer common winding</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WindingConnection.I">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">I</rdfs:label>
	<rdfs:domain rdf:resource="#WindingConnection"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Independent winding, for single-phase connections</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#WindingConnection.D">
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#attribute"/>
	<rdfs:label xml:lang="en">D</rdfs:label>
	<rdfs:domain rdf:resource="#WindingConnection"/>
	<cims:dataType rdf:resource="#"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1..1" />
	<cims:stereotype>enum</cims:stereotype>
	<rdfs:comment  rdf:parseType="Literal">Delta</rdfs:comment>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_StandardInterconnections">
	<rdfs:label xml:lang="en">StandardInterconnections</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">This section describes the standard interconnections for various types of equipment. These interconnections are understood by the application programs and can be identified based on the presence of one of the key classes with a relationship to the static power flow model: SynchronousMachineDynamics, AsynchronousMachineDynamics, EnergyConsumerDynamics or WindTurbineType3or4Dynamics. 

The relationships between classes expressed in the interconnection diagrams are intended to support dynamic behaviour described by either standard models or user-defined models.

In the interconnection diagrams, boxes which are black in colour represent function blocks whose functionality can be provided by one of many standard models or by a used-defined model. Blue boxes represent specific standard models.  A dashed box means that the function block or specific standard model is optional.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#RemoteInputSignal">
	<rdfs:label xml:lang="en">RemoteInputSignal</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Supports connection to a terminal associated with a remote bus from which an input signal of a specific type is coming.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_StandardInterconnections"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_InfGenericDataset">
	<rdfs:label xml:lang="en">InfGenericDataset</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">Generic descriptions of how domain data participates in datasets and alternate datasets.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#Package_InfOperationalLimits">
	<rdfs:label xml:lang="en">InfOperationalLimits</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">The description of computed or dynamic limits.
These classes would likely go into the OperationalLimits package.</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#ScheduledLimitValue">
	<rdfs:label xml:lang="en">ScheduledLimitValue</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">A limit that is applicable during a scheduled time period.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_InfOperationalLimits"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_InfSIPS">
	<rdfs:label xml:lang="en">InfSIPS</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
	<rdfs:comment rdf:parseType="Literal">System Integrity Protection Schemes (SIPS) (IEC terminology). Other names used are: Remedial Action Schemes (RAS) or System Protection Schemes (SPS)</rdfs:comment>
</rdf:Description>
	 <rdf:Description rdf:about="#PinTerminal">
	<rdfs:label xml:lang="en">PinTerminal</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Value associated with Terminal is used as compare.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_InfSIPS"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Package_CanonicalModel">
	<rdfs:label xml:lang="en">CanonicalModel</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
</rdf:Description>
	 <rdf:Description rdf:about="#Package_GenericDatasetCore">
	<rdfs:label xml:lang="en">GenericDatasetCore</rdfs:label>
	<rdf:type rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#ClassCategory"/>
</rdf:Description>
	 <rdf:Description rdf:about="#CIMDataObject">
	<rdfs:label xml:lang="en">CIMDataObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">A description of a version of instance data.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_GenericDatasetCore"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
	 <rdf:Description rdf:about="#CIMDataObject.Dataset">
	<rdfs:label xml:lang="en">Dataset</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Dataset containing the data objects.</rdfs:comment>
	<rdfs:domain rdf:resource="#CIMDataObject"/>
	<rdfs:range rdf:resource="#Dataset"/>
	<cims:inverseRoleName rdf:resource="#Dataset.AlternateCIMDataObject"/>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:1"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
<cims:stereotype>informative</cims:stereotype>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Dataset.AlternateCIMDataObject">
	<rdfs:label xml:lang="en">AlternateCIMDataObject</rdfs:label>
	<rdfs:comment  rdf:parseType="Literal">Data objects contained in the dataset.</rdfs:comment>
	<rdfs:domain rdf:resource="#Dataset"/>
	<rdfs:range rdf:resource="#CIMDataObject"/>
	<cims:inverseRoleName rdf:resource="#CIMDataObject.Dataset"/>
	<rdfs:label xml:lang="en">AlternateCIMDataObject</rdfs:label>
	<cims:multiplicity rdf:resource="http://iec.ch/TC57/1999/rdf-schema-extensions-19990926#M:0..n"/>
	<cims:AssociationUsed>No</cims:AssociationUsed>
	<rdf:type rdf:resource="http://www.w3.org/1999/02/22-rdf-syntax-ns#Property"/>
<cims:stereotype>informative</cims:stereotype>
	 </rdf:Description>
	 <rdf:Description rdf:about="#Dataset">
	<rdfs:label xml:lang="en">Dataset</rdfs:label>
	<rdfs:subClassOf rdf:resource="#IdentifiedObject"/>
	<rdfs:comment  rdf:parseType="Literal">Versioned instance of a model part.   This would correspond to a payload of instance data without a header.   The MRID would be used in audit trail, not in reusable script intended to work with new versions of data.
A dataset could be serialized multiple times and in multiple technologies, yet retain the same identity.</rdfs:comment>
	<cims:belongsToCategory rdf:resource="#Package_GenericDatasetCore"/>
	<cims:stereotype rdf:resource="http://iec.ch/TC57/NonStandard/UML#concrete"/>
	<rdf:type rdf:resource="http://www.w3.org/2000/01/rdf-schema#Class"/>
	 </rdf:Description>
</rdf:RDF>
